import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
// import MenuIcon from '@material-ui/icons/Menu';
import Avatar from '@material-ui/core/Avatar';
import { Link } from "react-router";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  navLink: {
    color: 'white',
  },
  title: {
    flexGrow: 1,
  },
}));

export default function ButtonAppBar() {
  const classes = useStyles();
  const { token: isLogged = false } = document.cookie;

  const login = () => {
    const { location } = window;
    location.href = `${location.origin}/login`
  };

  const logout = () => {

  };

  const renderMenu = () => {
    const onClikc = isLogged ? logout : login;
    const label = isLogged ? 'Logout' : 'Login';
    return (
      <Button color="inherit" onClick={onClikc}><Link className={classes.navLink} to="/login">{label}</Link></Button>
    );
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <Avatar />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Users records
          </Typography>
          {renderMenu()}
        </Toolbar>
      </AppBar>
    </div>
  );
}
