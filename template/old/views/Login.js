import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: '0 auto',
    minWidth: '300px',
  },
  input: {
    width: '300px',
    padding: '10px',
    textIndent: '10px',
  },
  Button: {
      backgroundColor: '#2196f3',
      color: 'white',
  }
}));

export default function BasicTextFields() {
  const classes = useStyles();

  return (
    <form className={classes.root} autoComplete="off" action="http://localhost:8080/login" method="post" >
      <TextField className={classes.input} required id="username" label="User" />
      <br />
      <TextField className={classes.input} required id="password" label="Password" type="password"/>
      <br />
      <Button type="submit" color="primary" >Login</Button>
    </form>
  );
}
