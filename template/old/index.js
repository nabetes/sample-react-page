import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import PublicNavBar from './layout/PublicNavBar';
import Home from './views/Home';
import Login from './views/Login';
import { makeStyles, ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { blue, blueGrey } from '@material-ui/core/colors';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: '50px',
  },
}));

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: blueGrey,
    // error?: PaletteColorOptions;
    // warning?: PaletteColorOptions;
    // info?: PaletteColorOptions;
    // success?: PaletteColorOptions;
    // type?: PaletteType;
    // tonalOffset?: PaletteTonalOffset;
    // contrastThreshold?: number;
    // common?: Partial<CommonColors>;
    // grey?: ColorPartial;
    // text?: Partial<TypeText>;
    // divider?: string;
    // action?: Partial<TypeAction>;
    // background?: Partial<TypeBackground>;
    // getContrastText?: (background: string) => string;
  }
});

const Container = () => {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <PublicNavBar />
        <div className={classes.root}>
          <Switch>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
      </Router>
    </ThemeProvider>);
}

ReactDOM.render(
  <React.StrictMode>
    <Container />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
