﻿angular.module("ntsLoginGlossary", [])
    .factory("ntsLoginGlossary", [function () {
        return {
            login: {
                ntRegisterSuccess: "El usuario <b>{0}</b> ha sido registrado exitosamente. Redirigiendo a la página principal.",
                ntsRecoverPasswordSuccess: "Los pasos a seguir para restablecer la cuenta serán enviados al correo <b>{0}</b>, si el mismo concuerda con la información de alguna de las cuentas de usuario."
            }
        };
    }]);