﻿angular.module("ntsURL", [])
    .provider("ntsURL", [function () {
        let $this = this,
            urlBase = "http://localhost:52239/api/";

        $this.data = {
            ntsServiceWorker: {
                file: "/ntsServiceWorker.js?v=0"
            },
            session: {
                getTokenBasedSocialMedia: urlBase + "security/getTokenBasedSocialMedia",
                getToken: urlBase + "security/getToken",
                logout: urlBase + "security/logOut"
            },
            login: {
                register: urlBase + "login/register",
                forgotPassword: urlBase + "login/forgotPassword"
            },
            settings: {
                getUserSettings: urlBase + "settings/getUserSettings" 
            },
            profile: {
                getUserSettings: urlBase + 'profile/getUserSettings'
            }
        };

        $this.$get = [function () {
            return $this.data;
        }]
    }]);