'use strict';
angular.module('notesApp', ['ui.router', 'oc.lazyLoad', 'ngSanitize',
    '$$glossary', '$$popupBlockerCheck', '$$exception', '$$modal', '$$backgroundFilter', "$$lazyRender",
    "ntsConfig", "ntsGlossary"])
    .config(['$ocLazyLoadProvider', "$locationProvider",
        function ($ocLazyLoadProvider, $locationProvider) {

            $locationProvider.hashPrefix('');

            $ocLazyLoadProvider.config({
                debug: false,
                events: true,
                modules: []
            });           
        }]);