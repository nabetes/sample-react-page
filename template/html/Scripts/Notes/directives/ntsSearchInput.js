﻿angular.module('notesApp')
    .directive("ntsSearchInput", ["$$scopeProto", "$$backgroundFilter", "ntsGlossary",
        function ($$scopeProto, $$backgroundFilter, ntsGlossary) {
            let $$tableFilter = new $$backgroundFilter(),
                glossary = ntsGlossary.ntsSearchInput,
                template = `<div class="nts-search-input">
                                <div class="chat__search">
                                    <div class="form-group">
                                        <input type="text"
                                               ng-model="::sv.input"
                                               ng-change="::onChange()"
                                               ng-minlength="ntsMinLength"
                                               ng-model-options="::{ debounce: 500 }"
                                               placeholder="{{::(ntsPlaceholder || '${glossary.inputPlaceHolder}')}}"
                                               class="form-control default-font-placeholder">
                                        <i class="form-group__bar"></i>
                                    </div>
                                </div>
                                <div class="progress">
                                    <div class="progress-bar"
                                         role="progressbar" style="width: 0" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>`;

            return {
                restrict: 'A',
                scope: {
                    ntsSearchInput: "@",
                    ntsDataOrigin: "&",
                    ntsDataTarget: "@",
                    ntsChange: "&",
                    ntsPlaceholder: "@",
                    ntsMinLength: "@"
                },
                transclude: true,
                replace: true,
                template: template,
                link: function ($scope, $element, $attrs, $ctrl, $transclude) {
                    let $$directive = null,
                        progressBar = null;
                    $$scopeProto.extend($scope.$parent);

                    function $$controller() {
                        $scope.sv = {
                            id: Date.now(),
                            input: ""
                        };

                        $scope.ntsMinLength = $scope.ntsMinLength || 3; 

                        $scope.onChange = function () {
                            $$directive.onChange();
                        };

                        progressBar = $element.find(`.progress-bar:first`)
                        if ($attrs.ntsSearchInput) {
                            $scope.$parent.$$set($attrs.ntsSearchInput, this);
                        }
                    }

                    $$controller.prototype.onChange = function () {
                        $$tableFilter.filter($scope.ntsDataOrigin(), $scope.sv.input, function ($$progress) {
                            setTimeout(function () {
                                progressBar.width(`${$$progress < 100 ? $$progress : 0}%`);
                            }, $$progress * 2);
                        }).then(function (data) {
                            $scope.$parent.$$set($attrs.ntsDataTarget, data);
                        });
                    }

                    return $$directive = new $$controller();
                }
            };
        }]);