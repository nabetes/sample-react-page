﻿'use strict';
angular.module("notesApp")
    .controller("indexController", ["$scope", "$window", "$q", "$location",
        "$$session", "$state", "$$stateDictionary", "$http", "$$message", "$$workerFactory", "$$exception",
        "ntsURL",
        function ($scope, $window, $q, $location,
            $$session, $$state, $$stateDictionary, $$http, $$message, $$workerFactory, $$exception,
            ntsURL) {
            let $$this = new $$controller();

            function $$controller() {
                $scope.sv = {
                    user: {},
                    mainReady: false,
                    appSettings: {},
                    menuCurrentSelection: {},

                    chats: [],
                    messages: [],
                    notifications: []
                };

                $scope.logOut = function () {
                    $$this.logOut();
                }
            }

            $$controller.prototype.getAppSettings = function () {
                return $$http.get(ntsURL.settings.getUserSettings);
            }

            $$controller.prototype.logOut = function () {
                $$session.logOut().then(function () {
                    $$message.notify({
                        type: "success"
                    });
                    $$message.notify({
                        type: "success",
                        message: "Chao!! Nos vemos!",
                        onClose: function () {
                            $$session.sendToLogin();
                        }
                    });
                });
            }

            $$controller.prototype.configureInfoPages = function () {
                /*Info pages could be configured*/
                let inforPages = ['support', 'aboutUs', 'contact', 'profile', "_"];
                for (let index = 0, length = inforPages.length, pageKey; index < length; index++) {
                    pageKey = inforPages[index];
                    $$stateDictionary.scripts[pageKey] = {
                        cache: true,
                        files: [`views/${pageKey}/${pageKey}.js?v=0`]
                    };
                    $$state.$$state(pageKey, {
                        url: `/${pageKey}`,
                        templateUrl: `views/${pageKey}/${pageKey}.html`,
                        controller: `${pageKey}Controller`,
                        resolve: $$state.ntsResolve(pageKey)
                    });
                }
            }

            $$controller.prototype.initialize = function () {
                return $$session.$$userRequest.promise.then(function (user) {
                    $scope.sv.user = user;
                    let authorizationToken = 'Basic ' + $$session.getToken(),
                        $$httpHeaders = {
                            'Authorization': authorizationToken,
                            'ntsHTTP': true
                        };

                    $$http.$$getHttpProvider().defaults.headers.common = $$httpHeaders;
                    $$workerFactory.$$setConfig({
                        $$httpHeaders: $$httpHeaders
                    });

                    $$this.getAppSettings().then(function (response) {
                        let appSettings = response.data.Data;
                        $$stateDictionary.$$menu = {};

                        for (let index = 0, length = appSettings.menu.length, option; index < length; index++) {
                            option = appSettings.menu[index];
                            if (option.visible) {
                                $$stateDictionary.$$menu[option.route] = option;
                            }
                        }

                        for (let index = 0, length = appSettings.scripts.length, config; index < length; index++) {
                            config = appSettings.scripts[index];
                            config.files = config.files.split(",");
                            $$stateDictionary.scripts[config.name] = config;
                        }

                        for (let index = 0, length = appSettings.routes.length, route = null; index < length; index++) {
                            route = appSettings.routes[index];
                            route.resolve = $$state.ntsResolve.apply(null, route.resolve.split(","));
                            $$state.$$state(route.state, route);
                        }

                        let routeKey = $location.$$path.replace("/", ""),
                            menuItem = $$stateDictionary.$$menu[routeKey]
                                || (routeKey ? { route: routeKey } : appSettings.menu[0]),
                            /*mobile view close rigth-menu*/
                            aside = $("#sidebar");

                        $$state.$$otherwise(appSettings.otherwise);
                        $$state.$$setConfig({
                            onChange: function (event, toState, toParams, fromState, fromParams) {
                                let menuItem = $$stateDictionary.$$menu[fromParams.menu || toState.state];
                                $scope.sv.menuCurrentSelection.class = "";

                                if (menuItem) {
                                    menuItem._route = toState.state;
                                    menuItem.class = "navigation__active";
                                    $scope.sv.menuCurrentSelection = menuItem;
                                }

                                if (aside.hasClass("toggled")) {
                                    $('[data-ma-action="aside-close"]:first').click();
                                }
                            }
                        });


                        //remove
                        $$stateDictionary.scripts["grades2"] = {
                            cache: false,
                            files: ["views/grades/grades2.js?v=0"],
                            name: "grades2",
                            serie: false
                        };
                        //remove
                        $$state.$$state("grades2", {
                            url: `/grades2`,
                            templateUrl: `views/grades/grades2.html`,
                            controller: `grades2Controller`,
                            resolve: $$state.ntsResolve("$$tables", "grades2")
                        });


                        $$state.go(menuItem.route);
                        $scope.sv.appSettings = appSettings;
                        $scope.sv.mainReady = true;                       
                    }, function (error) {
                        $(".page-loader").fadeOut();
                        setTimeout(function () {
                            $$exception.$$scope().sv.modal.onClose(function () {
                                $$session.logOut();
                                $$session.sendToLogin();
                            });
                        }, 0);
                        throw error;
                    });

                    $$this.configureInfoPages();
                });
            }

            $$this.initialize();
        }]);
