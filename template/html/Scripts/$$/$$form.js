﻿angular.module("$$form", ["$$glossary", "$$glossary", "$$exception"])
    .provider("$$form", [function () {
        var $this = this;
        $this.config = {
            formSuccessClass: "SSform-success",
            inputSuccessClass: "SSinput-success",
            formFailClass: "SSform-invalid",
            inputFailClass: "SSinput-invalid"
        };

        $this.setConfig = function (config) {
            Object.assign($this.config, config);
        }

        $this.$get = function () {
            return {
                config: $this.config
            }
        }
    }])
    .directive("$$input", ["$$exception", "$$glossary", "$$form", "$parse",
        function ($$exception, $$glossary, $$form, $parse) {
            return {
                restrict: 'A',
                transclude: false,
                replace: false,
                require: "^ngModel",
                scope: false,
                priority: 10,
                link: function ($scope, $element, $attr, $ngModel) {
                    /*$attr: {
                        $$input: "display value",
                        $attr['$${0}Message']: "validator message override",
                        $$inputSuccessClass: null,
                        $$inputFailClass: null
                    }*/
                    let inputEventId = ".$$input" + Date.now(),
                        getter = $parse($attr.ngModel),
                        setter = $parse($attr.ngModel).assign,
                        inputInvalid = "inputInvalid",
                        $inputContainer = $element.parents("[\\$\\$validate={0}]:first".$$stringFormat($attr.name)),
                        spanInfo = $(`<span 
                                    title="" 
                                    data-html="true"
                                    data-toggle="tooltip" 
                                    class="SSspan">`);

                    $inputContainer.append(spanInfo);
                    /*initialize the toolTip widget*/
                    spanInfo.tooltip();

                    $ngModel.$$spanInfo = spanInfo;
                    $ngModel.$$exception = new $$exception();
                    $ngModel.$$input = {
                        _addedClasses: {},
                        value: function (value) {
                            if (value !== undefined) {
                                $element.removeClass("form-control--active");
                                return setter($scope, value);
                            }
                            else
                                return getter($scope);
                        },
                        validate: function (config) {
                            $ngModel.$$exception.clean();
                            config = Object.assign({
                                inputSuccessClass: $attr.$$inputSuccessClass || $$form.config.inputSuccessClass,
                                inputFailClass: $attr.$$inputFailClass || $$form.config.inputFailClass
                            }, config);

                            for (let key in $ngModel._$$$validators) {
                                $ngModel._$$$validators[key]($ngModel.$modelValue, $ngModel.$viewValue);
                            }

                            if ($ngModel.$$exception.Exceptions.length) {
                                $ngModel.$setValidity(inputInvalid, false);
                                if (config.inputFailClass)
                                    $ngModel.$$input.addClass(config.inputFailClass);
                            }
                            else {
                                $ngModel.$setValidity(inputInvalid, true);
                                if (config.inputSuccessClass)
                                    $ngModel.$$input.addClass(config.inputSuccessClass);
                            }

                            return $ngModel.$$exception;
                        },
                        clean: function (clean) {
                            $ngModel.$$input.removeClass(Object.keys($ngModel.$$input._addedClasses).join(" "));
                            $ngModel.$$spanInfo.attr("data-original-title", null);
                            if (clean)
                                $ngModel.$$input.value(null);
                        },
                        addClass: function ($class) {
                            let classes = $class.split(" ");
                            $ngModel.$$input.removeClass(Object.keys($ngModel.$$input._addedClasses).join(" "));
                            for (let index = 0, length = classes.length; index < length; index++) {
                                $ngModel.$$input._addedClasses[classes[index]] = 1;
                            }
                            $inputContainer.addClass($class);
                        },
                        removeClass: function ($class) {
                            let classes = $class.split(" ");
                            for (let index = 0, length = classes.length; index < length; index++) {
                                delete $ngModel.$$input._addedClasses[classes[index]];
                            }
                            $inputContainer.removeClass($class);
                        }
                    };
                    $ngModel._$$$validators = {};

                    for (let key in $ngModel.$validators) {
                        wrapValidator(key, $ngModel.$validators[key]);
                    }

                    function wrapValidator(key, base) {
                        $ngModel._$$$validators[key] = function () {
                            let result = base.apply($ngModel, arguments);
                            if (!result) {
                                let message = $attr['$${0}Message'.$$stringFormat(key)] || $$glossary.$$form.validators[key];
                                $ngModel.$$exception.add(message, $attr.$$input);
                            }
                            return result;
                        }
                    }

                    /*Normal validators excetuted to many times*/
                    $ngModel.$validators = {};
                }
            }
        }])
    .directive("form", ["$$exception", "$$form", function ($$exception, $$form) {
        return {
            restrict: 'E',
            transclude: false,
            replace: false,
            require: "^form",
            scope: false,
            priority: 10,
            link: {
                pre: function ($scope, $element, $attr, $form) {
                    /*$attr: {
                        $$exceptionShow: true // Indicates if the validation process should build a popUp,
                        $$formSuccessClass: null,
                        $$inputSuccessClass: null,
                        $$formFailClass: null,
                        $$inputFailClass: null
                    }*/
                    let formInvalid = "formInvalid";
                    $form.$$exception = new $$exception();
                    $form.$$form = {
                        _addedClasses: {},
                        validate: function (config) {
                            $form.$$exception.Exceptions = [];
                            config = Object.assign({
                                formSuccessClass: $attr.$$formSuccessClass,
                                inputSuccessClass: $attr.$$inputSuccessClass,
                                formFailClass: $attr.$$formFailClass,
                                inputFailClass: $attr.$$inputFailClass
                            }, config);

                            for (let index = 0, length = $form.$$controls.length, $ngModel, validateConfig; index < length; index++) {
                                $ngModel = $form.$$controls[index];
                                validateConfig = {};
                                if (config.inputSuccessClass)
                                    validateConfig.inputSuccessClass = config.inputSuccessClass;
                                if (config.inputFailClass)
                                    validateConfig.inputFailClass = config.inputFailClass;
                                $form.$$exception.add($ngModel.$$input.validate(validateConfig));
                            }

                            if ($form.$$exception.Exceptions.length) {
                                $form.$setValidity(formInvalid, false);
                                if (config.formFailClass)
                                    $form.$$form.addClass(config.formFailClass);
                            }
                            else {
                                $form.$setValidity(formInvalid, true);
                                if (config.formSuccessClass)
                                    $form.$$form.addClass(config.formSuccessClass);
                            }

                            if ($form.$invalid) {
                                $form.$setDirty();
                                $form.$$form.addClass($$form.config.formFailClass);
                                debugger;
                                if (!($attr.$$exceptionShow === "0" || $attr.$$exceptionShow === "false"))
                                    $form.$$exception.show();
                            }

                            /*For input children*/
                            $element.trigger("$$validate");
                            return !$form.$invalid;
                        },
                        clean: function (clean) {
                            $form.$setPristine();
                            $form.$setSubmitted();
                            $form.$$exception.clean();
                            $form.$setValidity(formInvalid, true);
                            $element.trigger("$$clean", [{
                                clean: clean
                            }]);
                            $form.$$form.removeClass(Object.keys($form.$$form._addedClasses).join(" "));
                        },
                        addClass: function ($class) {
                            let classes = $class.split(" ");
                            $form.$$form.removeClass(Object.keys($form.$$form._addedClasses).join(" "));
                            for (let index = 0, length = classes.length; index < length; index++) {
                                $form.$$form._addedClasses[classes[index]] = 1;
                            }
                            $element.addClass($class);
                        },
                        removeClass: function ($class) {
                            let classes = $class.split(" ");
                            for (let index = 0, length = classes.length; index < length; index++) {
                                delete $form.$$form._addedClasses[classes[index]];
                            }
                            $element.removeClass($class);
                        },

                        _inputAddedClasses: {},
                        inputsClean: function () {
                            let $class = Object.keys($form.$$form._inputAddedClasses).join(" ");
                            for (let index = 0, length = $form.$$controls.length, $ngModel; index < length; index++) {
                                $ngModel = $form.$$controls[index];
                                $ngModel.$$input.removeClass($class);
                            }
                        },
                        inputsAddClass: function ($class) {
                            let classes = $class.split(" ");
                            for (let index = 0, length = classes.length; index < length; index++) {
                                $form.$$form._inputAddedClasses[classes[index]] = 1;
                            }
                            for (let index = 0, length = $form.$$controls.length, $ngModel; index < length; index++) {
                                $ngModel = $form.$$controls[index];
                                $ngModel.$$input.addClass($class);
                            }
                        },
                        inputsRemoveClass: function ($class) {
                            let classes = $class.split(" ");
                            for (let index = 0, length = classes.length; index < length; index++) {
                                delete $form.$$form._inputAddedClasses[classes[index]];
                            }
                            for (let index = 0, length = $form.$$controls.length, $ngModel; index < length; index++) {
                                $ngModel = $form.$$controls[index];
                                $ngModel.$$input.removeClass($class);
                            }
                        }
                    };

                    /*custom validation will be taking care for the directive*/
                    $element.attr("novalidate", true);
                    $element.addClass("$$form");

                    $element.on("submit.form", function (event) {
                        if (!$form.$$form.validate()) {
                            event.stopImmediatePropagation();
                            event.preventDefault();
                        }
                    });

                    let modal = $element.parents(".ui-modal:first");
                    if (modal.length) {
                        modal.on('hidden.bs.modal', function () {
                            $form.$$form.clean();
                        });
                    }
                }
            }
        }
    }])
    .directive('$$validate', ["$$form", "$$exception", "$$glossary",
        function ($$form, $$exception, $$glossary) {
            return {
                restrict: 'A',
                transclude: false,
                replace: false,
                require: "^form",
                scope: false,
                link: function ($scope, $inputContainer, $inputAttrs, $form) {
                    let $ngModel = $form[$inputAttrs.$$validate],
                        inputEventId = ".$$validate" + Date.now();

                    if (!$ngModel)
                        $$exception.addAndThrow($$glossary.$$form.mgsInputDoesntExist, $inputAttrs.$$validate);

                    $form.$$element.on("$$validate" + inputEventId, function (event) {
                        if ($form.$invalid && $ngModel.$invalid) {
                            $ngModel.$$spanInfo.attr("data-original-title", $ngModel.$$exception.Exceptions.join(""));
                        } else {
                            $ngModel.$$spanInfo.attr("data-original-title", null);
                        }
                    });

                    /*Clean event of the father*/
                    $form.$$element.on("$$clean" + inputEventId, function (event, data) {
                        $ngModel.$$input.clean(data.clean);
                    });

                    /*clean event of the input*/
                    $ngModel.$$element.on("focus" + inputEventId, function (event) {
                        $ngModel.$$input.clean();
                    });

                    /*Tooltip*/
                    $ngModel.$$spanInfo.on("click" + inputEventId, function () {
                        $ngModel.$$input.clean();
                    });
                }
            };
        }]);
