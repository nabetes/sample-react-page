﻿'use scrict';
angular.module('$$sessionFacebook', ['$$glossary', '$$exception', '$$popupBlockerCheck', '$$session', 'oc.lazyLoad'])
    .provider("$$facebook", ["$$sessionProvider", function ($$sessionProvider) {
        var $this = this;
        $this.$$config = {
            JDK: 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId={0}',
            appId: null,
            status: true,
            cookie: true,
            xfbml: true,
            version: "v2.11",
            tokenKey: null,
            picture: {
                width: 512,
                height: 512
            }
        };
        $this.$$setConfig = function (config) {
            $.extend($this.$$config, {
                tokenKey: $$sessionProvider.$$config.tokenKey || "facebook"
            }, config);
        }
        $this.$get = function () {
            return {
                config: $this.$$config
            }
        }
    }])
    .run(["$ocLazyLoad", "$q", "$window",
        "$$sessionManagerDictionary", "$$facebook", "$$exception", "$$popupBlockerCheck", "$$glossary", "$$session",
        function ($ocLazyLoad, $q, $window,
            $$sessionManagerDictionary, $$facebook, $$exception, $$popupBlockerCheck, $$glossary, $$session) {
            function setToken(token) {
                $window.localStorage.setItem($$facebook.config.tokenKey, token);
            }

            function getToken() {
                let token = $window.localStorage.getItem($$facebook.config.tokenKey);
                return ["undefined", "null"].indexOf(token) !== -1 ? null : token;
            }

            var media = {
                key: "facebook",
                _promise: null,
                login: function () {
                    let defer = $q.defer(),
                        exception = new $$exception();

                    if ($$popupBlockerCheck(exception)) {
                        defer.reject(exception);
                    } else
                        FB.login(function (response) {
                            if (response.status === "connected") {
                                defer.resolve(response);
                            }
                            else if (response.status)
                                defer.reject(new $$exception($$glossary.$$sessionFacebook.msgFail));
                            else
                                defer.reject(response);
                        }, { scope: 'email' });

                    return defer.promise;
                },
                logOut: function () {
                    var defer = $q.defer();
                    FB.logout(function (response) {
                        defer.resolve(response);
                    });
                    return defer.promise;
                },
                getUser: function () {
                    var defer = $q.defer();
                    FB.api(`/me?fields=name,email,picture.width(${$$facebook.config.picture.width}).height(${$$facebook.config.picture.height})`, function (res) {
                        if (!res || res.error)
                            return defer.reject(res);

                        let user = {
                            name: res.name,
                            email: res.email,
                            id: res.id,
                            provider: "facebook",
                            imageUrl: res.picture.data.url
                        };

                        if ($$session.$$config.onGetUser)
                            $$session.$$config.onGetUser(defer, user);
                        else
                            defer.resolve(user);
                    });
                    return defer.promise;
                },
                checkStatus: function (onTimeout) {
                    var defer = $q.defer();
                    FB.getLoginStatus(function (response) {
                        if (response.status === "connected")
                            defer.resolve(response);
                        else
                            defer.reject(response);
                    });

                    /*Sometimes facebook api doesnt resolve*/
                    setTimeout(function () {
                        if (defer.promise.$$state.status === 0) {
                            onTimeout();
                        }
                    }, 2000);
                    return defer.promise;
                },
                getToken: function () {
                    return getToken();
                },
                setToken: function (token) {
                    return setToken(token);
                },
                initialize: function () {
                    window.fbAsyncInit = function () {
                        try {
                            FB.init(Object.assign({}, $$facebook.config));
                            FB.AppEvents.logPageView();
                        } catch (ex) {
                            throw new $$$$exception(ex);
                        }
                    };

                    let defer = $q.defer();
                    $.getScript($$facebook.config.JDK.$$stringFormat($$facebook.config.appId))
                        .done(function (script, textStatus) {
                            defer.resolve();
                        })
                        .fail(function (jqxhr, settings, exception) {
                            defer.reject({
                                sessionMediaError: true,
                                code: "cant get facebook api"
                            });
                            media._promise = null;
                        });
                    return defer.promise;
                }
            };
            $$sessionManagerDictionary.facebook = media;
        }]);