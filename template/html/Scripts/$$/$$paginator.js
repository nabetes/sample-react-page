﻿angular.module("$$paginator", ["$$scopeProto", "$$glossary"])
    .directive('$$paginator', ['$window', "$rootScope", '$$scopeProto', '$$glossary', 
        function ($window, $rootScope, $$scopeProto, $$glossary) { 
            return {
                restrict: 'A',
                template: `<span>
                                <div class="dataTables_wrapper" ng-if="!$$rowsLength">
                                    <div class="dataTables_info"
                                         role="status"
                                         aria-live="polite" ng-bind="::$$glossary.msgQueryNoResults">
                                    </div>
                                </div>
                                <div class="dataTables_wrapper" ng-if="$$rowsLength">
                                    <div class="dataTables_info"
                                         role="status"
                                         aria-live="polite" ng-bind="$$glossary.msgQueryResults.$$stringFormat(_$$stardRow, _$$endRow, $$rowsLength)">
                                    </div>
                                    <div class="dataTables_info dataTables_paginate paging_simple_numbers">
                                        <a ng-show="$$currentPage > 1"
                                           class="paginate_button previous disabled"
                                           aria-controls="data-table"
                                           data-dt-idx="0"
                                           tabindex="0"
                                           ng-click="$$goToPage(1);">
                                            Anterior
                                        </a>
                                        <span>
                                            <a ng-show="_$$visibleRange.length && (_$$visibleRange[0] - 1) > 0 && _$$visibleRange.indexOf(_$$visibleRange[0] - 1) == -1"
                                               ng-hide="_$$visibleRange.length < 3"
                                               ng-click="$$goToPage(_$$visibleRange[0] - 1);"
                                               class="paginate_button"
                                               aria-controls="data-table"
                                               data-dt-idx="1"
                                               tabindex="0"
                                               title="Ver más">
                                                ...
                                            </a>
                                            <a ng-show="_$$visibleRange.length > 1"
                                               ng-repeat="number in _$$visibleRange"
                                               ng-class="'ui-paginate-btnPage ' + (number == $$currentPage ? 'current' : '')"
                                               ng-click="$$goToPage(number);"
                                               class="paginate_button "
                                               aria-controls="data-table"
                                               data-dt-idx="2" tabindex="0"
                                               ng-bind="number">
                                                2
                                            </a>
                                            <a ng-show="_$$visibleRange.length && _$$visibleRange[_$$visibleRange.length - 1] < _$$totalPages"
                                               ng-hide="_$$visibleRange.length < 3"
                                               ng-click="$$goToPage(_$$visibleRange[_$$visibleRange.length - 1] + 1);"
                                               class="paginate_button " aria-controls="data-table" data-dt-idx="5"
                                               tabindex="0"
                                               title="Ver más">
                                                ...
                                            </a>
                                        </span>
                                        <a ng-show="$$currentPage < _$$totalPages"
                                           class="paginate_button next"
                                           aria-controls="data-table"
                                           data-dt-idx="7"
                                           tabindex="0"
                                           ng-click="$$goToPage(_$$totalPages);">
                                            Next
                                        </a>
                                    </div>
                                </div>
                           </span>`,
                transclude: true,
                replace: true,
                scope: {
                    $$paginator: "@",
                    $$scope: "=?",
                    $$localStorageKey: "=?",
                    $$currentPage: "=?",
                    $$rowsLength: "@",
                    $$pageSize: "@",
                    $$limitto: "@",
                    $$hideInputPage: "@",
                    $$visiblePageRange: "=?",
                    $$onClick: "=?"
                },
                link: function ($scope, $element, $attrs, $ctrl, $trasnsclude) {
                    let paginator = null,
                        $$mainScope = $$scopeProto.extend($scope.$$scope ? $scope.$$scope() : $scope.$parent);

                    $scope.$$glossary = $$glossary.$$paginator;
                    $scope._$$totalPages = 0;
                    $scope._$$visibleRange = [];
                    $scope._$$stardRow = 0;
                    $scope._$$endRow = 0;
                    $attrs._$$onClick = undefined;
                    $attrs._$$rowLengthSourceName = $attrs.$$rowsLength.replace(".length", "");

                    /*#region public scope attr*/
                    $scope.$$localStorageKey = $attrs.$$localStorageKey || $attrs._$$rowLengthSourceName;
                    $scope.$$currentPage = 1;
                    $scope._$$currentPage = 1;
                    $scope.$$rowsLength = 0;
                    $scope.$$pageSize = $$mainScope.$$get($attrs.$$pageSize);//$window.localStorage.getItem($scope.$$localStorageKey);
                    $scope.$$limitto = -1;
                    $scope.$$hideInputPage = $attrs.$$hideInputPage;
                    /*#endregion public scope attr*/

                    if ((!$scope.$$pageSize) || isNaN($scope.$$pageSize)) {
                        $scope.$$pageSize = 10;
                    }

                    let propName;
                    angular.forEach(["pageSize", "currentPage", "limitto"], function (prop) {
                        propName = "$$" + prop;
                        if ($attrs[propName] == undefined) {
                            $attrs[propName] = propName + $attrs._$$rowLengthSourceName.replace(".", "");
                        }
                        if ($$mainScope.$$get($attrs[propName]) == undefined) {
                            $$mainScope.$$set($attrs[propName], $scope[propName]);
                        }
                    });
                   
                    $$mainScope.$watchGroup([$attrs.$$rowsLength, $attrs.$$pageSize], function (values) {
                        paginator.getTotalPages(parseInt(values[0]), parseInt(values[1]), $scope);
                        if ($scope.$$currentPage == 1) {
                            paginator.goToPage(1, $scope, $attrs, $$mainScope);
                        } else {
                            $$mainScope.$$set($attrs.$$currentPage, 1);
                        }
                    });

                    $$mainScope.$watch($attrs.$$currentPage, function (value) {
                        paginator.goToPage(parseInt(value), $scope, $attrs, $$mainScope);
                    });

                    $$mainScope.$$set($attrs.$$pageSize, $scope.$$pageSize);
                   
                    $scope.$$goToPage = function (number) {
                        paginator.goToPage(number);
                    }

                    $scope.$$changePageSize = function (size) {
                        paginator.changePageSize(size);
                    }

                    function $$paginator() {
                        if ($attrs.$$paginator) {
                            $$mainScope.$$set($attrs.$$paginator, this);
                        }
                    }

                    $$paginator.prototype.goToPage = function () {
                        if (number && number >= 1 && number <= $scope._$$totalPages) {
                            if (number == $scope.$$currentPage) {
                                paginator.goToPage(number, $scope, $attrs, $$mainScope);
                            } else {
                                $$mainScope.$$set($attrs.$$currentPage, number);
                            }
                        }
                    }

                    $$paginator.prototype.changePageSize = function (size) {
                        $window.localStorage.setItem($scope.$$localStorageKey, size);

                        this.getTotalPages($scope.$$rowsLength, size);
                        $scope._$$visibleRange = this.getPaginateVisibleRange(1, $scope._$$totalPages, $scope.$$visiblePageRange);
                        $scope._$$endRow = $scope._$$stardRow + $scope.$$pageSize - 1;
                        $$mainScope.$$set($attrs.$$pageSize, size);
                    }
                    
                    $$paginator.prototype.getTotalPages = function(rowsLength, pageSize) {
                        $scope.$$rowsLength = rowsLength;
                        $scope.$$pageSize = pageSize;

                        $scope._$$totalPages = (!rowsLength ? 1 : (rowsLength % pageSize == 0) ? rowsLength / pageSize
                            : parseInt((rowsLength / pageSize) + 1));
                    }

                    $$paginator.prototype.getPaginateVisibleRange = function(current, last, interval) {
                        interval = interval || 2;
                        var left = current - interval,
                            right = current + interval + 1,
                            range = [];

                        if (interval) {
                            for (let i = 1; i <= last; i++) {
                                if (i >= left && i < right) {
                                    range.push(i);
                                }
                            }
                        } else {
                            range = [current];
                        }

                        return range;
                    }

                    $$paginator.prototype.goToPage = function(value) {
                        $scope.$$currentPage = value;
                        $scope._$$currentPage = value;
                        $scope._$$visibleRange = this.getPaginateVisibleRange(value, $scope._$$totalPages, $scope.$$visiblePageRange);
                        this.execOnClick(value);
                    }

                    $$paginator.prototype.execOnClick = function(number) {
                        $scope._$$stardRow = (number == 1 ? 1 : (((number - 1) * $scope.$$pageSize) + 1));
                        $scope._$$endRow = $scope._$$stardRow + $scope.$$pageSize - 1;
                        if ($scope.$$rowsLength < $scope._$$endRow) {
                            $scope._$$endRow = $scope.$$rowsLength;
                        }

                        $scope.$$limitto = (-$scope.$$rowsLength + $scope._$$stardRow) - 1;

                        // Change the limitTo.
                        $$mainScope.$$set($attrs.$$limitto, $scope.$$limitto);

                        if ($attrs.$$onClick) {
                            $scope.$$onClick();
                        }
                    }

                    return paginator = new $$paginator();
                }
            };
        }]);
