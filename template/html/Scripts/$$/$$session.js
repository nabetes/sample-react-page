﻿"use strict";
angular.module('$$session', [])
    .provider('$$sessionManagerDictionary', function () {
        var dictionary = {};

        this.addMedia = function (key, value) {
            dictionary[key] = value;
        }

        this.$get = function () {
            return dictionary;
        };
    })
    .provider("$$session", [function () {
        let $$this = this;
        $$this.$$config = {
            tokenKey: "$$session",
            onGetUser: null,
            loginUrl: "login.html"
        };

        $$this.$$setConfig = function (config) {
            Object.assign($$this.$$config, config);
        }

        $$this.currentMedia = null;

        $$this.$get = ["$q", "$$sessionManagerDictionary", "$window", function ($q, $$sessionManagerDictionary, $window) {
            function onReady() {
                if (!$$this.currentMedia._promise) {
                    $$this.currentMedia._promise = $$this.currentMedia.initialize();
                } else if ($$this.currentMedia._promise.$$state.status === 1) {
                    return $q.when(1);
                }

                return $$this.currentMedia._promise;
            }

            function setKey(key) {
                $window.localStorage.setItem("$$sessionKey", key !== undefined ? key : $$this.currentMedia.key);
            }

            function getKey() {
                let key = $window.localStorage.getItem("$$sessionKey");
                return ["undefined", "null"].indexOf(key) !== -1 ? null : key;
            }

            function sendToLogin() {
                $window.location.href = $$this.$$config.loginUrl;
            }

            var $socialService = {
                sendToLogin: sendToLogin,
                login: function (key, config) {
                    $$this.currentMedia = $$sessionManagerDictionary[key];
                    setKey();
                    return onReady().then($$this.currentMedia.checkStatus)
                        .then(function (response, token) {
                            return response;
                        }, function (error) {
                            if (error && error.sessionMediaError)
                                throw error;
                            return $$this.currentMedia.login();
                        });
                },
                logOut: function () {
                    if ($$this.currentMedia) {
                        $$this.currentMedia.setToken(null);
                        setKey(null);
                        return $$this.currentMedia.logOut().catch(function () {
                            sendToLogin();
                        });
                    }
                    return $q.when(1);
                },
                checkStatus: function (key, onTimeout) {
                    key = key || getKey();
                    $$this.currentMedia = $$sessionManagerDictionary[key];
                    if ($$this.currentMedia) {
                        setKey();
                        return $$this.currentMedia.checkStatus(onTimeout);
                    }

                    return $q.reject();
                },
                getUser: function () {
                    if ($$this.currentMedia)
                        return $$this.currentMedia.getUser();
                    return $q.reject();
                },
                getToken: function () {
                    if ($$this.currentMedia)
                        return $$this.currentMedia.getToken();
                },
                setToken: function (value) {
                    if ($$this.currentMedia)
                        return $$this.currentMedia.setToken(value);
                },
                parseToken: function (token) {
                    let user = null;
                    try {
                        let base64Url = token.split('.')[1];
                        let base64 = base64Url.replace('-', '+').replace('_', '/');
                        user = JSON.parse(window.atob(base64));
                    } finally {
                        return user;
                    }
                },
                getKey: function () {
                    if ($$this.currentMedia)
                        return $$this.currentMedia.key;
                },
                $$config: $$this.$$config,
                $$setConfig: $$this.$$setConfig,
                initialize: function () {
                    var statusRequest = null,
                        key = getKey();

                    if (key) {
                        $$this.currentMedia = $$sessionManagerDictionary[key];
                        $$this.currentMedia._promise = $$this.currentMedia.initialize();
                        statusRequest = $$this.currentMedia._promise;
                    } else {
                        /*If there is no default*/
                        statusRequest = $q.when(1);
                    }

                    return statusRequest;
                }
            };
            return $socialService;
        }];
    }]);