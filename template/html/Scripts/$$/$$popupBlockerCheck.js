﻿angular.module("$$popupBlockerCheck", ["$$glossary"])
    .factory("$$popupBlockerCheck", ["$$glossary", function ($$glossary) {
        let $isBlock = undefined;

        /**
        * Returns true if popUps are blocked
        */
        return function checkBlocker(exception) {
            let isBlock = false;
            if ($isBlock === false)
                return isBlock;

            var openWin = window.open("http://www.google.com", "directories=no,height=100,width=100,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
            if (isBlock = !openWin) {
                $isBlock = isBlock;
                if (exception) {
                    exception.add($$glossary.$$popupBlockerCheck.msgPopUpIsBlock);
                }
            } else {
                $isBlock = false;
            }
            openWin.close();
            return isBlock;
        }
    }]);