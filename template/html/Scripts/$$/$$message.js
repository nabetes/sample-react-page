﻿/*required: bootstrap-notify.min.js*/
angular.module('$$message', ["$$modal", "$$glossary"])
    .factory('$$message', ["$document", "$rootScope", "$compile", "$$glossary",
        function ($document, $rootScope, $compile, $$glossary) {
            let modalTemplate = `<div   id="SSmessage-modal"
                                        $$modal="::$$modal"
                                        $$size="::sv.size" 
                                        $$title="::sv.title">
                                    <div class="modal-body"
                                            ng-bind-html="::sv.message">
                                    </div>
                                    <footer class="modal-footer">
                                        <button type="button" 
                                                class="btn btn-default btn-primary waves-effect"
                                                data-dismiss="modal" 
                                                ng-bind="::(sv.isConfirm ? sv.btnCancelLabel: sv.btnOkLabel)">
                                        </button>
                                        <button type="button"
                                                class="btn btn-primary waves-effect"
                                                data-dismiss="modal" 
                                                ng-if="::sv.isConfirm"   
                                                ng-click="sv.yes();"
                                                ng-bind="::sv.btnOkLabel">
                                        </button>
                                    </footer>
                                </div>`,
                notificationTemplate = `<div data-notify="container"
                                             class="SSmessage-notification alert alert-dismissible alert-{0} alert--notify swing animated"
                                             role="alert">
                                            <h6 class="alert-heading">{1}</h6>
                                            <p class="mb-0"><span class="SSmessage-notification-icon" data-notify="icon"></span>{2}</p>
                                            <a href="{3}" target="{4}" data-notify="url"></a>

                                            <a aria-hidden="true"
                                               data-notify="dismiss"
                                               class="alert--notify__close">
                                                <i class="zmdi zmdi-close zmdi-hc-fw"></i>
                                            </a>
                                        </div>`;

            function $$message() {
                this.$scope = null;
                this.$element = null;
                this.$$modal = null;
                this.sv = null;

                this.$scope = $rootScope.$new();
                this.$scope.sv = {};
                this.$scope.$$modal = {};

                this.$element = $compile(modalTemplate)(this.$scope);
                $document.find("body:first").append(this.$element);

                this.sv = this.$scope.sv;
                this.$$modal = this.$scope.$$modal;
            }

            $$message.prototype._refreshConfig = function (params, defaultParams) {
                if (typeof (params) === "string") {
                    params = {
                        message: params
                    }
                }
                $.extend(this.$scope.sv, defaultParams, params);
                this.$element = $compile(modalTemplate, this.$scope);
                this.$$modal.open();
            }

            $$message.prototype.alert = function (params) {
                this._refreshConfig(params, {
                    title: $$glossary.$$message.mdTitleAlert,
                    size: 'sm',
                    message: "",
                    isConfirm: false,
                    yes: undefined,
                    no: undefined,
                    btnOkLabel: $$glossary.$$message.btnOkAlert
                });
            }

            $$message.prototype.confirm = function (params) {
                this._refreshConfig(params, {
                    title: $$glossary.$$message.mdTitleCofirm,
                    size: 'sm',
                    message: "",
                    isConfirm: true,
                    yes: undefined,
                    no: undefined,
                    btnOkLabel: $$glossary.$$message.btnOkConfirm,
                    btnCancelLabel: $$glossary.$$message.btnCancelConfirm
                });
            }

            $$message.NotificationTypes = {
                //Default                        
                "warning": {
                    title: $$glossary.$$message.notification.warning.title,
                    type: "warning",
                    message: $$glossary.$$message.notification.warning.message,
                    icon: "zmdi zmdi-alert-triangle zmdi-hc-fw",
                },
                "success": {
                    title: $$glossary.$$message.notification.success.title,
                    type: "success",
                    message: $$glossary.$$message.notification.success.message,
                    icon: "zmdi zmdi-check-circle zmdi-hc-fw",
                },
                "danger": {
                    title: $$glossary.$$message.notification.danger.title,
                    type: "danger",
                    message: $$glossary.$$message.notification.danger.message,
                    icon: "zmdi zmdi-close-circle zmdi-hc-fw",
                },
                "info": {
                    title: $$glossary.$$message.notification.info.title,
                    type: "info",
                    message: $$glossary.$$message.notification.info.message,
                    icon: "zmdi zmdi-info-outline zmdi-hc-fw",
                }
            };

            $$message.prototype.notify = function (config) {
                if (typeof config === "string")
                    config = {
                        message: config
                    };
                
                config = $.extend({
                    title: null,
                    type: null,
                    message: null,
                    icon: null,
                    from: null,
                    align: null,
                    animIn: null,
                    animOut: null,
                    url: null,
                    onShow: null,
                    onShown: null,
                    onClose: null,
                    onClosed: null,
                    allow_dismiss: true,
                    timer: 1000,
                    delay: 1000,
                    spacing: 10,
                    z_index: 1031,
                    mouse_over: false,
                    template: notificationTemplate
                }, $$message.NotificationTypes[config.type || "info"], config);

                $.notify({
                    icon: config.icon,
                    title: config.title,
                    message: config.message,
                    url: config.url
                }, config);
            }
            
            return new $$message;
        }]);
