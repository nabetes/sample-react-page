﻿'use scrict';
angular.module('$$sessionGoogle', ['$$glossary', '$$exception', '$$popupBlockerCheck', '$$session', 'oc.lazyLoad'])
    .provider("$$google", ["$$sessionProvider", function ($$sessionProvider) {
        var $this = this;
        $this.$$config = {
            JDK: "https://apis.google.com/js/platform.js",
            client_id: null,
            scope: "email",
            tokenKey: "$$google",
            onUserReady: null,
            picture: {
                size: 512
            }
        };
        $this.$$setConfig = function (config) {
            $.extend($this.$$config, {
                tokenKey: $$sessionProvider.$$config.tokenKey || "google",
            }, config);
        }
        $this.$get = function () {
            return {
                config: $this.$$config
            }
        }
    }])
    .run(["$q", "$window",
        "$$sessionManagerDictionary", "$$google", "$ocLazyLoad", "$$exception", "$$glossary", "$$popupBlockerCheck", "$$session",
        function ($q, $window,
            $$sessionManagerDictionary, $$google, $ocLazyLoad, $$exception, $$glossary, $$popupBlockerCheck, $$session) {
            function setToken(token) {
                $window.localStorage.setItem($$google.config.tokenKey, token);
            }

            function getToken() {
                let token = $window.localStorage.getItem($$google.config.tokenKey);
                return ["undefined", "null"].indexOf(token) !== -1 ? null : token;
            }

            var media = {
                key: "google",
                _promise: null,
                login: function () {
                    let defer = $q.defer(),
                        exception = new $$exception();

                    if ($$popupBlockerCheck(exception)) {
                        defer.reject(exception);
                    } else
                        gapi.auth2.getAuthInstance()
                            .signIn({ scope: 'email' })
                            .then(function (response) {
                                let user = {
                                    name: response.w3.ig,
                                    email: response.w3.U3,
                                    id: response.El,
                                    provider: "google",
                                    imageUrl: `${response.w3.Paa}?sz=500`
                                };

                                if ($$session.$$config.onGetUser)
                                    $$session.$$config.onGetUser(defer, user);
                                else
                                    defer.resolve(response);
                            }, function (result) {
                                defer.reject(new $$exception($$glossary.$$sessionGoogle.msgFail));
                            });

                    return defer.promise;
                },
                logOut: function () {
                    let defer = $q.defer();

                    gapi.auth2.getAuthInstance()
                        .signOut()
                        .then(function (result) {
                            defer.resolve(result);
                        }, function (result) {
                            defer.reject(result);
                        });

                    return defer.promise;
                },
                getUser: function () {
                    let defer = $q.defer();

                    gapi.auth2.getAuthInstance()
                        .then(function (data) {
                            if (data.isSignedIn.Ab) {
                                let res = data.currentUser.get().getBasicProfile();
                                let user = {
                                    name: res.getName(),
                                    email: res.getEmail(),
                                    id: res.getId(),
                                    provider: "google",
                                    imageUrl: `${res.getImageUrl()}?sz=${$$google.config.picture.size}`
                                };

                                if ($$session.$$config.onGetUser)
                                    $$session.$$config.onGetUser(defer, user);
                                else
                                    defer.resolve(response);
                            } else
                                defer.reject();
                        });

                    return defer.promise;
                },
                checkStatus: function () {
                    let defer = $q.defer();

                    gapi.auth2.getAuthInstance()
                        .then(function (data) {
                            if (data.isSignedIn.Ab)
                                defer.resolve();
                            else
                                defer.reject();
                        }, function (data) {
                            defer.reject(data);
                        });

                    return defer.promise;
                },
                getToken: function () {
                    return getToken();
                },
                setToken: function (token) {
                    setToken(token);
                },
                initialize: function () {
                    let defer = $q.defer();
                    $.getScript($$google.config.JDK)
                        .done(function (script, textStatus) {
                            gapi.load('auth2', function () {
                                gapi.auth2.init(Object.assign({}, $$google.config)).then(function () {
                                    defer.resolve();
                                });
                            });
                        })
                        .fail(function (jqxhr, settings, exception) {
                            defer.reject({
                                sessionMediaError: true,
                                code: "cant get google api"
                            });
                            media._promise = null;
                        });
                    return defer.promise;
                }
            };
            $$sessionManagerDictionary.google = media;
        }]);