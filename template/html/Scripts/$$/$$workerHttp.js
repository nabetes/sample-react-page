﻿var $http = new function () {  
    function baseRequest(resolve, reject) {
        let request = new XMLHttpRequest();
        request.onload = function () {
            if (request.status === 200)
                return resolve(request.responseText);
            reject(request.response);
        };
        request.onerror = function () {
            reject(request.response);
        };                       
        return request;
    };

    function addHeaders(request) {
        request.setRequestHeader('Content-Type', 'application/json');
        for (var header in self.$$httpHeaders) {
            request.setRequestHeader(header, self.$$httpHeaders[header]);
        }
    }

    this.get = function (url, parameters) {
        return new Promise(function (resolve, reject) {
            let request = baseRequest(resolve, reject);
            request.open("GET", url, true);
            addHeaders(request);
            request.send(null);
        });
    };

    this.post = function (url, parameters) {
        return new Promise(function (resolve, reject) {
            let request = baseRequest(resolve, reject);
            request.open("POST", url, true);
            addHeaders(request);
            request.send(JSON.stringify(parameters));
        });
    };

    this.all = function (promises) {
        return Promise.all(promises);
    }
};