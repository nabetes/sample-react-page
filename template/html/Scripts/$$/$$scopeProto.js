﻿/*
** Autor: Johnny Quesada
* Service that probides facilities to modifier the value of attributes on the rootscope by the key name
*/
angular.module("$$scopeProto", [])
    .factory('$$scopeProto', ["$parse", function ($parse) {

        function $$scopeProto() {
            $.extend(this, $$scopeProto.prototype);
        }

        $$scopeProto.prototype.$$set = function (key, value) {
            $parse(key).assign(this, value);
        }

        $$scopeProto.prototype.$$get = function (key) {
            return $parse(key)(this);
        }

        return {
            extend: function ($scope) {
                $$scopeProto.call($scope);
                return $scope;
            }
        };
    }])