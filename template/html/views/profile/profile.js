﻿angular.module("notesApp")
    .controller("profileController", ["$scope", "$state", "$http", "$rootScope", "$timeout",
        "user", "ntsURL", "$$message", 
        function ($scope, $state, $$http, $rootScope, $timeout,
            user, ntsURL, $$message) {
            let $$this = new $$controller();

            function $$controller() {
                $scope.sv = {
                    lazyrender: null,

                    user: user,
                    about: {}, //title,description, linkedin, tel, email,

                    model: {},
                    editingProfile: false
                };

                $scope.editProfile = function () {
                    $$this.editProfile();
                };

                $scope.cancelEditProfile = function () {
                    $$this.cancelEditProfile();
                };

                $scope.editAboutSave = function () {
                    $$this.editAboutSave();
                };                
            }

            $$controller.prototype.editProfile = function () {
                $scope.frmEditAbout.$$form.clean();
                $scope.sv.editingProfile = true;
                $scope.sv.model.user = $.extend({}, $scope.sv.user);
                $scope.sv.model.about = $.extend({}, $scope.sv.about);
                

            }

            $$controller.prototype.cancelEditProfile = function () {
                $scope.sv.editingProfile = false;
                $scope.sv.model = {};

            }

            $$controller.prototype.editAboutSave = function () {
                $scope.sv.user = $.extend($scope.sv.user, $scope.sv.model.user);
                $scope.sv.about = $.extend($scope.sv.about, $scope.sv.model.about);

                $timeout(function () {                                  
                    $scope.sv.editingProfile = false;
                }, 1000);

                $$message.notify({
                    type: "success"
                });
            }
                        
            $$controller.prototype.initialize = function () {
                $$http.get(ntsURL.profile.getUserSettings)
                    .then(function (request) {      
                        $scope.sv.about = request.data.Data;
                    })
                    .finally(function () {
                        $scope.sv.lazyrender();
                    });
            }

            $$this.initialize();
        }]);