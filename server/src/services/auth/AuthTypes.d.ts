declare namespace Auth {
  export interface AuthParameters {
    username: String;
    password: String;
  }

  export interface Auth {
    'First name': string;
    'One-time password': string;
    Identifier: string,
    Username: String;
    _id: string;
  }
}
