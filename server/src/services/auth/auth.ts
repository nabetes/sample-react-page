import Exception from '@src/services/Exception/Exception';
import environmentConfig from '@src/services/environmentConfig';
import jsonwebtoken from 'jsonwebtoken';
import { MongoClient } from 'mongodb';
import HTTPStatusCode from '../Exception/exceptionFixtures';

export class Auth {

  login = async (authParameters: Auth.AuthParameters): Promise<string> => {
    let client;

    try {
      client = await MongoClient.connect(environmentConfig.CONNECTION_STRING_MONGO, { useUnifiedTopology: true });
      const database = client.db('sample');
      const usersCollection = await database.collection('users');
      const { username, password } = authParameters;

      const user = await usersCollection.findOne({
        'One-time password': password,
        Username: username,
      });

      if (!user) {
        new Exception('Usuario y contraseña incorrecto', {
          statusCode: HTTPStatusCode.NOT_FOUND,
        }).throw();
      }

      return jsonwebtoken.sign(user as Auth.Auth, environmentConfig.JWT_SECRET_KEY);
    } finally {
      client?.close();
    }
  }

}

export default new Auth();
