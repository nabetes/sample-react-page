type ErrorCode = 'UNHANDLED_ERROR' | 'HANDLED_ERROR';

type StatusCodeLabels = 'INTERNAL_SERVER_ERROR' | 'UNAUTHORIZED' | 'NOT_FOUND';

type StatusCodes = 500 | 401 | 404;

type IHTTPStatusCode = { [key in StatusCodeLabels]: StatusCodes };

type ErrorDetail = string | string[] | { [key: string]: string[] };

interface IExceptionConfig {
  details: ErrorDetail;
  errorCode: ErrorCode;
  message: string;
  shouldLogIncident: boolean;
  statusCode: StatusCodes;
  wrappedError: Error | null;
}

interface IExceptionProvider extends Error, IExceptionConfig {
  errorCode: ErrorCode;

  quietException: boolean;

  shouldLogIncident: boolean;

  statusCode: StatusCodes;

  wrappedError: Error | null;

  details: ErrorDetail;

  // new(exception: Error | string | IExceptionProvider, config: Partial<IExceptionConfig> = {}) : IExceptionProvider;

  setConfig(config: Partial<IExceptionConfig>): IExceptionProvider;

  markLikeHandled(): IExceptionProvider;

  throw(): void;
}
