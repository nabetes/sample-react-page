export const HTTPStatusCode: IHTTPStatusCode = {
  INTERNAL_SERVER_ERROR: 500,
  NOT_FOUND: 404,
  UNAUTHORIZED: 401,
};

export default HTTPStatusCode;
