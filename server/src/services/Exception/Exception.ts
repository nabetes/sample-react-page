import ExceptionProvider from './ExceptionProvider';

export class Exception extends ExceptionProvider implements IExceptionProvider {}

export default Exception;
