import { HTTPStatusCode } from '@src/services/Exception/exceptionFixtures';

export const CUSTOM_ERROR_MESSAGE = 'Ups! ocurrio algo inesperado';

export class ExceptionProvider extends Error implements IExceptionProvider {

  errorCode: ErrorCode = 'HANDLED_ERROR';

  quietException = true;

  shouldLogIncident = false;

  statusCode = HTTPStatusCode.INTERNAL_SERVER_ERROR;

  wrappedError: Error | null = null;

  details: ErrorDetail;

  message: string;

  constructor(exception: string | Error | IExceptionProvider, config?: Partial<IExceptionConfig>) {
    super();
    this.message = CUSTOM_ERROR_MESSAGE;
    this.details = [CUSTOM_ERROR_MESSAGE];

    if (exception) {
      if (Object.prototype.hasOwnProperty.call(exception, 'details')) {
        const {
          errorCode, details, quietException, shouldLogIncident, statusCode, wrappedError, message,
        } = exception as ExceptionProvider;

        this.message = message;
        this.details = details;
        this.errorCode = errorCode;
        this.quietException = quietException;
        this.statusCode = statusCode;
        this.wrappedError = wrappedError;
        this.shouldLogIncident = shouldLogIncident;
      } else if (typeof exception === 'string') {
        this.quietException = false;
        this.details = exception;
      } else {
        this.errorCode = 'UNHANDLED_ERROR';
        this.shouldLogIncident = true;
        this.wrappedError = exception;
        this.message = exception.message;
      }
    }

    if (config) this.setConfig(config);
  }

  setConfig(config: Partial<IExceptionConfig> = {}): IExceptionProvider {
    Object.keys(config).forEach((key) => {
      (this as any)[key] = (config as any)[key];
    });

    return this;
  }

  markLikeHandled(): IExceptionProvider {
    this.errorCode = 'HANDLED_ERROR';
    return this;
  }

  throw() {
    // eslint-disable-next-line no-throw-literal
    throw this;
  }

}

export default ExceptionProvider;
