interface IEnvironmentConfig {
  JWT_SECRET_KEY: string;
  NODE_ENV: string;
  PORT: number;
  SITE_URL: string;
  CONNECTION_STRING_MONGO: string;
}
