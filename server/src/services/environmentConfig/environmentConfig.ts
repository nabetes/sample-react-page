import dotenv from 'dotenv';

const config = dotenv.config().parsed as unknown as IEnvironmentConfig;

export default config;
