import {
  CaraCruz, GetNumeroRandom, PrintGuerreroStatus, PrintProgressTitle, PrintSingleLine, nombresBase,
} from './ArenaTools';
import Guerrero from './Guerrero';

/**
  * Arena en la que pelan los guerreros
  */
export class Arena {

  static NumeroTorneos: number;

  CantidadGuerreros: number = 6;

  NumeroRonda: number;

  GuerrerosInicialesColeccion: IGuerrero[];

  /**
      * Metodo que construye la arena para una cantidad especifica de guerreros
      * @param {number} Cantidad Cantidad de guerreros que lucharan, minimo 5.
      */
  // @ts-ignore
  constructor(pCantidadGuerreros) {
    this.CantidadGuerreros = pCantidadGuerreros || 5;
    // @ts-ignore
    this.GuerrerosInicialesColeccion = [];
    this.NumeroRonda = 0;
    this.Inicializar();
  }

  /**
      * Inicializa la arena
      */
  private Inicializar() {
    Arena.NumeroTorneos += 1;
    if (this.CantidadGuerreros < 5) {
      this.CantidadGuerreros = 5;
    }

    this.CrearGuerreros();
    PrintProgressTitle(`Inicia el torneo ${Arena.NumeroTorneos > 1 ? `#${Arena.NumeroTorneos}` : ''}`);
    PrintSingleLine(`Numero de participantes: ${this.CantidadGuerreros}`);
  }

  /**
     * Metodo que inicia las peleas entre guerreros
     * @param {number} Cantidad Cantidad de guerreros que lucharan, minimo 5.
     */
  CrearGuerreros() {
    let tempCollection = nombresBase.concat([]);
    const nombresBaseLength = nombresBase.length;

    for (let numero = 1; numero <= this.CantidadGuerreros; numero += 1) {
      this.GuerrerosInicialesColeccion.push(new Guerrero(
        numero,
        tempCollection.pop() + (numero > nombresBaseLength ? `#${numero}` : ''),
      ));
      if (!tempCollection.length) {
        tempCollection = nombresBase.concat([]);
      }
    }
  }

  /**
      * Enfrenta a muerte a 2 guerreros.
      * @param {guerrero} guerrero1
      * @param {guerrero} guerrero2
      */
  // @ts-ignore
  EnfrentarGuerrerosPares(guerrero1: Guerrero, guerrero2: Guerrero, moneda: boolean = false) {
    let primerGuerrero;
    let segundoGuerrero;

    if (moneda) {
      if (CaraCruz()) {
        primerGuerrero = guerrero1;
        segundoGuerrero = guerrero2;
      } else {
        primerGuerrero = guerrero2;
        segundoGuerrero = guerrero1;
      }
    } else {
      primerGuerrero = guerrero1;
      segundoGuerrero = guerrero2;
    }

    segundoGuerrero = primerGuerrero.atacarEnemigo(segundoGuerrero);
    if (primerGuerrero && segundoGuerrero) {
      return this.EnfrentarGuerrerosPares(segundoGuerrero, primerGuerrero);
    }

    return segundoGuerrero || primerGuerrero;
  }

  /**
      * Enfrenta la coleccion de guerreros
      * @param {array} guerreros coleccion de guerreros que se enfrentaran
      */
  // @ts-ignore
  EnfrentarGuerrerosColeccion(guerreros) {
    const sobrevivientes = [];
    let guerrero1;
    let guerrero2;
    let enfrentamientoNumero = 0;
    let index;

    this.NumeroRonda += 1;
    PrintProgressTitle(`Ronda #${this.NumeroRonda}`);
    // @ts-ignore
    guerreros.forEach((guerrero) => {
      PrintGuerreroStatus(guerrero);
    });
    PrintSingleLine('\n');

    while (guerreros.length) {
      enfrentamientoNumero += 1;

      index = GetNumeroRandom(0, guerreros.length - 1);
      [guerrero1] = guerreros.splice(index, 1);

      index = GetNumeroRandom(0, guerreros.length - 1);
      [guerrero2] = guerreros.splice(index, 1);

      PrintSingleLine(`    Enfrentamiento #${this.NumeroRonda}-${enfrentamientoNumero}`);
      if (guerrero2) {
        PrintSingleLine(`      ${guerrero1.nombre} vs ${guerrero2.nombre}`);
        sobrevivientes.push(this.EnfrentarGuerrerosPares(guerrero1, guerrero2, true));
      } else {
        PrintGuerreroStatus(guerrero1, 'Avanza sin combatir');
        sobrevivientes.push(guerrero1);
      }
    }

    if (sobrevivientes.length === 1) {
      return sobrevivientes[0];
    }

    return this.EnfrentarGuerrerosColeccion(sobrevivientes);
  }

  /**
      * Comienza los enfrentamientos
      */
  IniciarTorneo() {
    const ganador = this.EnfrentarGuerrerosColeccion(this.GuerrerosInicialesColeccion.concat([]));

    PrintProgressTitle('Fin de los enfrentamientos');
    PrintSingleLine('Ganador: ');
    PrintGuerreroStatus(ganador, '... fin');
  }

}

export default Arena;
