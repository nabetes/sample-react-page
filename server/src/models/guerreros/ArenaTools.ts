import Guerrero from './Guerrero/GuerreroProvider';

export const ataqueMinimo = 15;
export const ataqueMaximo = 30;
export const nombresBase = ['Kim posible', 'Goku', 'Johnny Bravo', 'Maestro Roshi', 'Chewbacca'];

export function PrintSingleLine(texto: string) {
  // eslint-disable-next-line no-console
  console.log(texto);
}

export function PrintGuerreroStatus(guerrero: Guerrero, nota = '') {
  PrintSingleLine(`  => Guerrero: ${guerrero.nombre}... Vida: ${guerrero.vida}, Ataque: ${guerrero.ataque}. ${nota}`);
}

/**
 * Obtiene un numero random entre un rango de numeros
 */
export function GetNumeroRandom(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Lanza una moneda
 * @returns moneda true => cara || false = cruz
 */
export function CaraCruz() {
  return Math.random() >= 0.5;
}

export function PrintProgressTitle(texto: string) {
  PrintSingleLine('\n=================================>>');
  // @ts-ignore
  PrintSingleLine(this, texto);
  PrintSingleLine('<<=================================\n\n');
}
