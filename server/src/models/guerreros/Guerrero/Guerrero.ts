import GuerreroProvider from '@src/models/guerreros/Guerrero/GuerreroProvider';

export class Guerrero extends GuerreroProvider implements IGuerrero {}

export default Guerrero;
