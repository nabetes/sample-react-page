interface IGuerrero {

  uniqueIdentifier: number;

  nombre: string;

  ataque: number;

  vida: number;

  // constructor(index: number, nombre: string) {
  //   this.uniqueIdentifier = index;
  //   this.nombre = nombre;
  //   this.ataque = GetNumeroRandom(ataqueMinimo, ataqueMaximo);
  //   this.vida = 100 - this.ataque;
  // }

  /**
  * Metodo que aplica un ataque de un guerrero sobre otro
  */
  atacarEnemigo(pEnemigo: IGuerrero): IGuerrero | null;

}
