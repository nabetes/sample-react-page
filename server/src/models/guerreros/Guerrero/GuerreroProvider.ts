import {
  GetNumeroRandom,
  PrintSingleLine,
  ataqueMaximo,
  ataqueMinimo,
} from '@src/models/guerreros/ArenaTools';

export class Guerrero implements IGuerrero {

  uniqueIdentifier: any;

  nombre: any;

  ataque: number;

  vida: number;

  constructor(index: number, nombre: string) {
    this.uniqueIdentifier = index;
    this.nombre = nombre;
    this.ataque = GetNumeroRandom(ataqueMinimo, ataqueMaximo);
    this.vida = 100 - this.ataque;
  }

  /**
  * Metodo que aplica un ataque de un guerrero sobre otro
  */
  atacarEnemigo(pEnemigo: Guerrero): Guerrero | null {
    const { nombre: enemigoName, vida: enemigoVida } = pEnemigo;
    const sangria = '          * ';
    const supperHitProbability = GetNumeroRandom(1, 10);
    const superHitFlag = GetNumeroRandom(1, supperHitProbability) === supperHitProbability;
    const superHitBonus = superHitFlag ? GetNumeroRandom(3, 15) : 0;
    const currentAtaque = superHitFlag ? (this.ataque + superHitBonus) : GetNumeroRandom(0, this.ataque);
    const vidaRestante = enemigoVida - currentAtaque;

    PrintSingleLine(`${sangria}${this.nombre} ataca a ${enemigoName} => `);

    if (superHitFlag) {
      PrintSingleLine(`${sangria}SuperHitBonus! Ataque+${superHitBonus}`);
    }

    PrintSingleLine(`${sangria}Daño total: ${currentAtaque}`);

    if (vidaRestante <= 0) {
      PrintSingleLine(`${sangria}${enemigoName} Fue eliminado.......\n\n`);
      return null;
    }

    if (superHitFlag) {
      PrintSingleLine(`${sangria}${enemigoName} Fue gravemente lastimado, vida actual: ${vidaRestante}.......\n\n`);
    } else {
      PrintSingleLine(`${sangria}${enemigoName} Fue lastimado, vida actual: ${vidaRestante}.......\n\n`);
    }

    // eslint-disable-next-line no-param-reassign
    pEnemigo.vida = vidaRestante;

    return pEnemigo;
  }

}

export default Guerrero;
