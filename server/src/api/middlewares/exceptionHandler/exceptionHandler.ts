import { Request, Response } from 'express';
import Exception from '@src/services/Exception/Exception';
import environmentConfig from '@src/services/environmentConfig';

export const exceptionHandler = (error: Error, _req: Request, res: Response, next: () => void) => {
  if (error) {
    const exception = new Exception(error);
    const { shouldLogIncident, statusCode } = exception;

    if (shouldLogIncident) {
      // log;
    }

    if (environmentConfig.NODE_ENV === 'DEV' && exception.wrappedError) {
      const { stack, message } = exception.wrappedError as Error;

      res.json({
        ...exception,
        wrappedError: {
          message,
          stack,
        },
      });
    } else {
      res.json({
        ...exception,
      });
    }

    res.statusCode = statusCode;
    res.end();
    return;
  }

  next();
};

export default exceptionHandler;
