import Exception from '@src/services/Exception/Exception';
import envConfig from '@src/services/environmentConfig';
import jsonwebtoken from 'jsonwebtoken';
import { Request, Response } from 'express';
import { Routers } from '@src/api/routers/RoutersTypes';
import { HTTPStatusCode } from '@src/services/Exception/exceptionFixtures';

export const authorization = (req: Request, _res: Response, next: () => void) => {
  try {
    const currentUser = jsonwebtoken.verify(req.headers.authorization as string, envConfig.JWT_SECRET_KEY);

    (req as unknown as Routers.IAuthorizedRequest).$User = currentUser as Auth.Auth;
    next();
  } catch (error) {
    new Exception(error).setConfig({
      details: 'Usuario no tiene permisos',
      statusCode: HTTPStatusCode.UNAUTHORIZED,
    }).throw();

  }
};

export default authorization;
