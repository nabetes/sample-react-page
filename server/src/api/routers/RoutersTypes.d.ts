import { ParamsDictionary, Request, Response } from 'express-serve-static-core';

declare namespace Routers {
  interface IAuthorizedRequest<P = ParamsDictionary, ResBody = any, ReqBody = any, ReqQuery = qs.ParsedQs> extends Request {
    $User: Auth.Auth;
  }

  type Callback = (req: IAuthorizedRequest, res: Response, next: () => void) => void;
}
