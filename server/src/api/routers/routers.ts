import express from 'express';
import loginRouter from '@src/api/routers/login';

export const routers = ({ app }: { app: express.Application }) => {
  app.use('/login', loginRouter);
};

export default routers;
