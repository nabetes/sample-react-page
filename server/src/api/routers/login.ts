import auth from '@src/services/auth';
import exceptionHandler from '@src/api/middlewares/exceptionHandler';
import { Router } from 'express';
import { createAuthorizedRouterCallback } from '@src/api/routers/routerFixtures';

export const router = Router();

router.post('/', async (req, res) => {
  // eslint-disable-next-line no-debugger
  debugger;
  const { body } = req;

  try {
    const userToken = await auth.login(body);

    res.json(userToken);
  } catch (error) {
    exceptionHandler(error, req, res, () => {});
  }
});

router.post('/validate', createAuthorizedRouterCallback((req, res) => {
  res.json(req.$User);
}));

export default router;
