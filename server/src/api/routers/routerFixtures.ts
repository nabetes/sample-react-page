import { Request, Response } from 'express';
import { Routers } from './RoutersTypes';
import authorization from '@src/api/middlewares/authorization';

export const createAuthorizedRouterCallback = (callback: Routers.Callback) => [authorization, (
  req: Request,
  res: Response,
  next: () => void,
) => callback(req as unknown as Routers.IAuthorizedRequest, res, next)];

export default createAuthorizedRouterCallback;
