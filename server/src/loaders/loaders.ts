import cors from 'cors';
import envConfig from '@src/services/environmentConfig';
import exceptionHandler from '@src/api/middlewares/exceptionHandler';
import express from 'express';
import { routers } from '@src/api/routers/';

export const loaders = ({ app }: { app: express.Application }) => {
  app.use(express.json());
  app.use(cors({
    origin: envConfig.SITE_URL,
  }));

  app.use(exceptionHandler);

  routers({ app });

  app.listen(envConfig.PORT, () => {
    // eslint-disable-next-line no-console
    console.log(`http://localhost:${envConfig.PORT}/`);
  });
};

export default loaders;
