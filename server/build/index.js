"use strict";
var cors = require('cors');
var express = require('express');
var jsonwebtoken = require('jsonwebtoken');
var app = express();
app.use(express.json());
app.use(express.urlencoded());
app.use(cors());
app.post('/login', function (req, res) {
    res.json({
        username: 'johnny quesada',
        edad: 25,
        email: 'johnny-qc@hotmail.com',
    });
});
app.listen(8080, function () {
    console.log('server ready');
});
