import * as IGlobalState from 'global-state-factory/lib/GlobalStoreTypes';
import GlobalState from 'global-state-factory';

export class GlobalStateFactory<
  IState,
  IActions extends IGlobalState.IActionCollection<IState> | null = null
> extends GlobalState<IState, IActions> implements IGlobalState.IGlobalState<IState, IActions> {
  
}

export default GlobalStateFactory;
