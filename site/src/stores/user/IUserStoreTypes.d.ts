declare namespace IUser {
  interface Metadata {
    username: string;
    email: string;
  }
}
