import { StateSetter } from 'global-state-factory/lib/GlobalStoreTypes';
import GlobalStateFactory from 'services/globalState';
import parseJwt from 'services/tools/parseJwt';

const initialState: IUser.Metadata = {
  email: '',
  username: '',
};

const store = new GlobalStateFactory('', null, 'USER_META_DATA');

const _useUserMetadata = store.getHook();
const _useUserMetadataDecoupled = store.getHookDecoupled();

export const useUserMetadata = (): [IUser.Metadata, StateSetter<string>
] => {
  const [jsonState, setter] = _useUserMetadata();

  if (!jsonState) return [initialState, setter];

  const userData = parseJwt(jsonState);
  const userMetaData: IUser.Metadata = userData;

  userMetaData.username = userData.Username;
  userMetaData.email = `${userData.Username}@hotmail.com`;

  return [userMetaData, setter];
};

export const useUserMetadataDecoupled = (): [() => IUser.Metadata | null, StateSetter<string>] => {
  const [getJsonState, setter] = _useUserMetadataDecoupled();

  return [(): IUser.Metadata | null => {
    const jsonState = getJsonState();

    if (!jsonState) return null;

    const userData = parseJwt(jsonState);
    const userMetaData: IUser.Metadata = userData;

    userMetaData.username = userData.Username;
    userMetaData.email = `${userData.Username}@hotmail.com`;

    return userMetaData;
  }, setter];
};

(window as any).useUserMetadataDecoupled = useUserMetadataDecoupled;
