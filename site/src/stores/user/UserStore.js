"use strict";
exports.__esModule = true;
exports.useUserMetadataDecoupled = exports.useUserMetadata = void 0;
var globalState_1 = require("services/globalState");
var parseJwt_1 = require("services/tools/parseJwt");
var initialState = {
    email: '',
    username: ''
};
var store = new globalState_1["default"]('', null, 'USER_META_DATA');
var _useUserMetadata = store.getHook();
var _useUserMetadataDecoupled = store.getHookDecoupled();
exports.useUserMetadata = function () {
    var _a = _useUserMetadata(), jsonState = _a[0], setter = _a[1];
    if (!jsonState)
        return [initialState, setter];
    var userData = parseJwt_1["default"](jsonState);
    var userMetaData = userData;
    userMetaData.username = userData.Username;
    userMetaData.email = userData.Username + "@hotmail.com";
    return [userMetaData, setter];
};
exports.useUserMetadataDecoupled = function () {
    var _a = _useUserMetadataDecoupled(), getJsonState = _a[0], setter = _a[1];
    return [function () {
            var jsonState = getJsonState();
            if (!jsonState)
                return [initialState, setter];
            var userData = parseJwt_1["default"](jsonState);
            var userMetaData = userData;
            userMetaData.username = userData.Username;
            userMetaData.email = userData.Username + "@hotmail.com";
            return userMetaData;
        }, setter];
};
window.useUserMetadataDecoupled = exports.useUserMetadataDecoupled;
