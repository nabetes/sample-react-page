(() => {
  const windowAccess = window as unknown as { onSubmit: (event: any) => void, $: any };
  const jquery = windowAccess.$;

  windowAccess.onSubmit = async (event: any) => {
    try {
      event.preventDefault();

      const values: { name: string, value: string }[] = [...jquery(event.target).find('input').map((_index, { name, value }) => ({ name, value }))];
      const auth: { username: string, password: string } = values.reduce((acumulator, { name, value }) => ({
        ...acumulator,
        [name]: value,
      }), {} as any);

      const response = await fetch('http://localhost:8080/login', {
        body: JSON.stringify(auth),
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'POST',
      });

      const result: any = await response.json();

      if (result.errorCode) {
        // eslint-disable-next-line no-alert
        throw result;
      }

      localStorage.setItem('USER_META_DATA', JSON.stringify(result));
      // eslint-disable-next-line no-restricted-globals
      location.href = location.origin;
    } catch (error) {
      if (error.details) {
        // eslint-disable-next-line no-alert
        alert(error.details);
      }
    }
  };
})();
