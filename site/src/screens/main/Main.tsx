import Header from 'screens/main/header';
import LeftMenu from 'screens/main/leftMenu';
import Chat from 'screens/main/chat';

export default function Main() {
  return (
    <main className="main">
      <Header />
      <LeftMenu />
      <Chat />
    </main>
  );
}
