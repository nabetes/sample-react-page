import { useUserMetadata } from 'stores/user';

const zmdiMoreVertStyle: any = {
  position: 'absolute',
  right: '-8px',
  top: '15px',
  zIndex: -1,
};

export default function LeftMenu() {
  const [{ email, username }] = useUserMetadata();

  return (
    <aside id="sidebar" className="sidebar">
      <div className="scrollbar-inner">
        <div className="user">
          <div className="user__info" data-toggle="dropdown">
            <img className="user__img" src="https://cdn.iconscout.com/icon/free/png-256/avatar-370-456322.png" alt="" />
            <div>
              <div className="user__name">{username}</div>
              <div className="user__email">{email}</div>
            </div>
          </div>
          <div className="dropdown-menu">
            <a className="dropdown-item" target="_parent" href="#/profile">Perfil</a>
            {/* <a className="dropdown-item" href="">Configuración</a> */}
            <a className="dropdown-item" href="/#" ng-click="::logOut();">Cerrar sesión</a>
          </div>
          <i className="zmdi zmdi-more-vert actions__item" style={zmdiMoreVertStyle} />
        </div>
        <ul className="navigation">
          {[1, 2, 3].map((item) => (
            <li
              key={item}
              ng-repeat="item in ::sv.appSettings.menu"
              ng-show="item.visible"
              ng-click="::menuClick(item);"
              className="item.class"
            >
              <a href="/#/{{::item.route}}">
                <i className="zmdi ::item.icon" />
                &nbsp;item
                {' '}
                {1}
              </a>
            </li>
          ))}

          {/* <li className="navigation__sub">
                <a href=""><i className="zmdi zmdi-view-week"></i> Example sub menu</a>
                <ul>
                    <li><a href="hidden-sidebar.html">Hidden Sidebar</a></li>
                </ul>
            </li>

            <li>
                <a href="index-template.html">
                    <i className="zmdi zmdi-format-underlined"></i> Template
                </a>
            </li> */}
        </ul>
      </div>
    </aside>
  );
}
