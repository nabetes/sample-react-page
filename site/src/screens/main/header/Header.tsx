import logo from 'assets/favicon.ico';

export default function Header() {
  return (
    <header className="header">
      <div className="navigation-trigger hidden-xl-up" data-ma-action="aside-open" data-ma-target=".sidebar">
        <div className="navigation-trigger__inner">
          <i className="navigation-trigger__line" />
          <i className="navigation-trigger__line" />
          <i className="navigation-trigger__line" />
        </div>
        <div className="header__logo hidden-sm-down clickable">
          <h1>
            <a href={logo}>Notes</a>
          </h1>
        </div>
        <form className="search">
          {/* <div className="search__inner">
              <input ng-disabled="!sv.mainReady"
                  type="text" className="search__text" placeholder="Buscar..." />
              <i className="zmdi zmdi-search search__helper" data-ma-action="search-close"></i>
          </div> */}
        </form>
      </div>
    </header>
  );
}
