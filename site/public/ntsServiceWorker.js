﻿// let ntsConnected = true;

// self.addEventListener('message', function (event) {
//     console.log("SW Received Message: " + event.data);
// });

// self.addEventListener('install', function (event) {
//     event.waitUntil(
//         self.skipWaiting()
//             .then(caches.open('notes-v1').then(function (cache) {
//                 return cache.addAll([
//                     //"/Styles/notes/notes.min.css?v=1",
//                     "/demo/img/profile-pics/1.jpg",
//                     "/demo/img/profile-pics/2.jpg",
//                     "/Scripts/bundles/template.min.js?v=1",
//                     "/Scripts/bundles/angularAll.min.js?v=1",
//                     //"/Scripts/bundles/notes.js?v=1",
//                     "/Scripts/Notes/modules/fakeData.js?v0",
//                     "/demo/img/profile-pics/3.jpg",
//                     "/demo/img/profile-pics/4.jpg",
//                     "/demo/img/profile-pics/5.jpg",
//                     "/demo/img/profile-pics/7.jpg",
//                     "/demo/img/profile-pics/8.jpg",
//                     "/demo/img/profile-pics/6.jpg",
//                     "/demo/img/profile-pics/9.jpg",
//                     "/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css",
//                     "/vendors/bower_components/animate.css/animate.min.css",
//                     "/vendors/bower_components/jquery.scrollbar/sass/jquery.scrollbar.min.css",
//                     "/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css",
//                     "/fonts/roboto/Roboto-Regular-webfont.woff",
//                     "/vendors/bower_components/material-design-iconic-font/dist/fonts/Material-Design-Iconic-Font.woff2?v=2.2.0",
//                     "/favicon.ico?v=1"
//                 ]);
//             }))
//     );
// });

// self.addEventListener('activate', function (event) {
//     //var cacheWhitelist = ['notes-v1' + Date.now()];//if version changes clean the cache. should be a request
//     //event.waitUntil(
//     //    caches.keys().then(function (cacheNames) {
//     //        return Promise.all(
//     //            cacheNames.map(function (cacheName) {
//     //                if (['notes-v1' + Date.now()].indexOf(cacheName) == -1) {
//     //                    return caches.delete(cacheName);
//     //                }
//     //            }).concat([self.clients.claim()])// Become available to all pages
//     //        )
//     //    })
//     //);
//     event.waitUntil(self.clients.claim()); // Become available to all pages
// });

// function _clean() {
//     caches.keys().then(function (cacheNames) {
//         return Promise.all(
//             cacheNames.map(function (cacheName) {
//                 if (['notes-v1' + Date.now()].indexOf(cacheName) === -1) {
//                     return caches.delete(cacheName);
//                 }
//             }).concat([self.clients.claim()])// Become available to all pages
//         )
//     })
// }

// function reject(event, body, status) {
//     return new Response(JSON.stringify(body || {
//         ntsServiceWorker: true,
//         code: "no internet",
//         Exceptions: ["Se ha perdido la conexion a internet"]
//     }), {
//             statusText: "no internet",
//             status: status || 503,
//             headers: {
//                 'Content-Type': 'application/json'
//             }
//         });
// }

// function sendMessage(data) {
//     clients.matchAll().then(function (clients) {
//         for (let index = 0, length = clients.length; index < length; index++) {
//             clients[index].postMessage(data);
//         }
//     });
// }

// self.addEventListener('fetch', function (event) {
//     /*POST Is not allowed in the WorkerService*/
//     if (event.request.method !== "POST") {
//         /**
//         * If the request if some ntsHTTP no HTML,
//         * we should try get the resource from internet first,
//         * ottherwise try to get the last request, but let the app know if offline
//         */
//         if (event.request.headers.get("ntsHTTP")
//             && event.request.url.indexOf(".html") === -1) {
//             event.respondWith(
//                 fetch(event.request.url, {
//                     method: event.request.method,
//                     headers: event.request.headers
//                 }).then(function (response) {
//                     if (!ntsConnected) {
//                         ntsConnected = true;
//                         sendMessage({
//                             code: "reconnected"
//                         });
//                     }
//                     let responseClone = response.clone();
//                     caches.open('notes-v1').then(function (cache) {
//                         cache.put(event.request, responseClone);
//                     });
//                     return response;
//                 })
//                     .catch(function (error) {
//                         return caches.match(event.request)
//                             .then(function (response) {
//                                 ntsConnected = false;
//                                 if (response !== undefined) {
//                                     /*Is working with cache, should notified the user.*/
//                                     sendMessage({
//                                         ntsServiceWorker: true,
//                                         code: "cache",
//                                         ntsBlock: false,
//                                         Exceptions: ["Se esta trabajando con cache"]
//                                     });
//                                     return response;
//                                 }

//                                 let result;
//                                 if (event.request.method === "GET") {
//                                     result = {
//                                         ntsServiceWorker: true,
//                                         code: "no internet",
//                                         ntsBlock: true,
//                                         Exceptions: ["No se puede obtener información del servidor de notes, <b>se ha perdido la conexión a internet.</b>"]
//                                     };
//                                 } else {
//                                     result = {
//                                         ntsServiceWorker: true,
//                                         ntsBlock: true,
//                                         code: "no internet",
//                                         Exceptions: ["No se puede acceder al servidor de notes, <b>se ha perdido la conexión a internet.</b>"]
//                                     };
//                                 }

//                                 sendMessage(result);
//                                 return reject(event, result);
//                             });
//                     })
//             );
//         }
//         else if (event.request.url.indexOf("facebook") === -1) {
//             /*todo: just cache https*/
//             //event.respondWith(
//             //    caches.match(event.request)
//             //        .then(function (response) {
//             //            // caches.match() always resolves
//             //            // but in case of success response will have value
//             //            if (response !== undefined) {
//             //                return response;
//             //            } else {
//             //                return fetch(event.request).then(function (response) {
//             //                    if (!ntsConnected) {
//             //                        ntsConnected = true;
//             //                        sendMessage({
//             //                            code: "reconnected"
//             //                        });
//             //                    }
//             //                    let responseClone = response.clone();
//             //                    caches.open('notes-v1').then(function (cache) {
//             //                        cache.put(event.request, responseClone);
//             //                    });
//             //                    return response;
//             //                }).catch(function () {
//             //                    /*Will not add the icos, should be reviewed*/
//             //                    return reject(event, {
//             //                        ntsServiceWorker: true,
//             //                        ntsBlock: false,
//             //                        code: "no internet",
//             //                        Exceptions: ["<b>Se ha perdido la conexión a internet.</b>"]
//             //                    });
//             //                });
//             //            }
//             //        }));
//         }
//     }
// });


// /*falsebook FACEBOOK BLOCKS THE FENCH*/
// //event.respondWith(
// //    fetch(event.request.url)
// //        .catch(function (error) {
// //            console.log("from service: ", event.request.url);
// //            return reject(event, {
// //                ntsServiceWorker: true,
// //                ntsBlock: false,
// //                code: "cant get facebook api"
// //            });
// //        })        );

