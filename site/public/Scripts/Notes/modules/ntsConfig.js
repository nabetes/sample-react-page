﻿angular.module("ntsConfig", ['ngSanitize',
    "$$session", "$$state", "$$exception", "$$http", "$$form", "$$message", "$$worker",
    "$$sessionGoogle", "$$sessionFacebook",
    "ntsSession", "ntsURL", "ntsGeneralGlossary", "ntsSpinner", "ntsServiceWorker"])
    .config(["$$sessionProvider", "$$googleProvider", "$$facebookProvider", "$$formProvider",
        "ntsSessionProvider", "ntsURLProvider",
        function ($$sessionProvider, $$googleProvider, $$facebookProvider, $$formProvider,
            ntsSessionProvider, ntsURLProvider) {

            $$sessionProvider.$$setConfig({
                getTokenURL: ntsURLProvider.data.session.getTokenBasedSocialMedia,
                tokenKey: "ntsSession"
            });

            $$facebookProvider.$$setConfig({
                appId: "2451415131595306"
            });

            $$googleProvider.$$setConfig({
                client_id: "353081789428-ndgpvpjn89pupcke2c2hhm4qfsb2p18h.apps.googleusercontent.com"
            });

            $$formProvider.setConfig({
                inputFailClass: "has-danger",
                inputSuccessClass: "has-success"
            });
        }])
    .run(["$q", "$window", "$document",
        "$$session", "$state", "$$stateDictionary", "$$exception", "$http",
        "ntsSession", "ntsURL", "ntsGeneralGlossary", "ntsSpinner", "ntsServiceWorker",
        function ($q, $window, $document,
            $$session, $$state, $$stateDictionary, $$exception, $$http,
            ntsSession, ntsURL, ntsGlossary, ntsSpinner) {

            /*This promise will process indexController initialize*/
            $$session.$$userRequest = $q.defer();

            $$http.$$setConfig({
                onStard: function (callback, url, data, config) {
                    config = Object.assign({
                        $$showLoading: true,
                        $$loadingDelay: 1000
                    }, config);

                    if (config.$$showLoading)
                        setTimeout(function () {
                            if ($$http.$$requests.length) {
                                ntsSpinner.delay.show(new $$exception(ntsGlossary.ntsSpinner.tlpDelay).getHtml());
                            }
                        }, config.$$loadingDelay);

                    return callback(url, data, config);
                },
                onFinish: function (some) {
                    ntsSpinner.delay.hide();
                }
            });

            /**
            * Custom handled of the services worker errors.
            */
            $$exception.$$setConfig({
                showTrace: true,
                onError: function (error, cause) {
                    if (error.constructor === $$exception)
                        return error.show();

                    let $$error = {},
                        facebookErrorMessage = "cant get facebook api",
                        googleErrorMessage = "cant get google api",
                        facebookKey = "facebook",
                        googleKey = "google",
                        sessionErrorBaseMessage = "No se puede acceder al servidor de <b>{0}</b>, se ha perdido la conexión a internet.",
                        sessionError = false,
                        sessionApiKey = null;

                    if (!error.indexOf)
                        $$exception.addAndThrow({
                            innerException: error
                        });

                    if (error.indexOf("Possibly unhandled rejection:") !== -1)
                        $$error = JSON.parse(error.substring(error.indexOf("{"), error.length));

                    if ($$error.Exceptions)
                        $$exception.addAndThrow($$error);

                    if ($$error.data && $$error.data.Exceptions)
                        $$exception.addAndThrow($$error.data);

                    if ($$error.status === -1)
                        $$exception.addAndThrow("En este momento no se puede acceder al servidor de <b>notes</b>… Inténtelo mas tarde.");

                    if ($$error.status === 503)
                        $$exception.addAndThrow($$error.data);

                    if ($$error.code) {
                        switch ($$error.code) {
                            case facebookErrorMessage:
                                sessionError = true;
                                sessionApiKey = facebookKey;
                                break;
                            case googleErrorMessage:
                                sessionError = true;
                                sessionApiKey = googleKey;
                                break;
                        }
                    }

                    if (sessionError) {
                        /*Try access offline by using local token*/
                        $$session.checkStatus(ntsSession.config.tokenKey)
                            .then(initialize, function () {
                                new $$exception().addAndThrow(sessionErrorBaseMessage.$$stringFormat(sessionApiKey));
                            });
                    } else if ($$error.code === "no internet") {
                        let modal = $$exception.show($$error.data);
                        modal.onClose(function () {
                            ntsSpinner.delay.show();
                        });
                        /*Close automatically*/
                        setTimeout(function () {
                            modal.close();
                        }, 5000);
                    } else {
                        $$exception.show(error);
                    }
                }
            });

            $$session.$$setConfig({
                onGetUser: function (defer, user) {
                    $$http.post(ntsURL.session.getTokenBasedSocialMedia, user)
                        .then(function (request) {
                            $$session.setToken(request.data.Data);
                            defer.resolve($$session.parseToken(request.data.Data));
                        })
                        .catch(function (error) {
                            let token = $$session.getToken();
                            if (token) {
                                defer.resolve($$session.parseToken(token));
                            } else {
                                defer.reject(error);
                            }
                        });
                }
            });

            function initialize() {
                $$session.initialize()
                    .then(function (request) {
                        return $$session.checkStatus(undefined, initialize)
                            .then(function () {
                                if ($window.location.pathname === "/login.html") {
                                    return $window.location.href = window.location.origin + "/#/";
                                }

                                let $$user;
                                $$session.getUser().then(function (user) {
                                    $$session.$$userRequest.resolve($$user = user);
                                }).catch(function (error) {
                                    $(".page-loader").fadeOut();
                                    setTimeout(function () {
                                        $$exception.$$scope().sv.modal.onClose(function () {
                                            $$session.logOut();
                                            $$session.sendToLogin();
                                        });
                                    }, 0);
                                    throw error;
                                });

                                $$state.ntsResolve = function () {
                                    var $argumengs = arguments,
                                        $base = $$state.$$resolve.apply($$state, $$.toArray($argumengs));
                                    $base["user"] = function () {
                                        return $q.when($$user);
                                    };
                                    return $base;
                                };
                            }, function () {
                                if ($window.location.pathname !== "/login.html") {
                                    $$session.sendToLogin();
                                }
                            });
                    });
            }

            initialize();
        }]);