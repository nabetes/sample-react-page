﻿angular.module("ntsGlossary", [])
    .factory("ntsGlossary", [function () {
        return {
            ntsSearchInput: {
                inputPlaceHolder: "Buscar..."
            },
            ntsSpinner: {                
                tlpDelay: "Algo esta tardando mas de lo esperado..."
            }
        };
    }]);