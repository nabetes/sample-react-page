﻿'use scrict';
angular.module('ntsSession', ['oc.lazyLoad', '$$session', '$$exception', "ntsURL"])
    .provider("ntsSession", ["$$sessionProvider", function ($$sessionProvider) {
        var $$this = this;
        $$this.$$config = {
            tokenKey: $$sessionProvider.tokenKey || "ntsSession"
        };
        $$this.$$setConfig = function (config) {
            Object.assign($$this.$$config, config);
        }
        $$this.$get = function () {
            return {
                config: $$this.$$config
            }
        }
    }])
    .factory("ntsSessionModel", [function () {
        let model = null;//user, pass
        return {
            set: function (value) {
                model = value;
            },
            get: function () {
                return model;
            }
        };
    }])
    .run(["$ocLazyLoad", "$q", "$window",
        "$$sessionManagerDictionary", "$http", "$$exception", "$$session",
        "ntsSessionModel", "ntsSession", "ntsURL",
        function ($ocLazyLoad, $q, $window,
            $$sessionManagerDictionary, $$http, $$exception, $$session,
            ntsSessionModel, ntsSession, ntsURL) {
            function setToken(token) {
                $window.localStorage.setItem(ntsSession.config.tokenKey, token);
            }

            function getToken() {
                let token = $window.localStorage.getItem(ntsSession.config.tokenKey);
                return ["undefined", "null"].indexOf(token) !== -1 ? null : token;
            }

            function readToken() {
                return $$session.parseToken(getToken());
            }
   
            var media = {
                key: ntsSession.config.tokenKey,
                _promise: null,
                login: function () {
                    return $$http.post(ntsURL.session.getToken, ntsSessionModel.get(), {
                        addToken: false
                    }).then(function (response) {
                        setToken(response.data.Data);
                        return response;
                    });
                },
                logOut: function () {
                    setToken(null);
                    return $$http.get(ntsURL.session.logout)
                        .then(function (request) {
                            return request;
                        });
                },
                getUser: function () {
                    let user = readToken();
                    if (user) {
                        return $q.when({
                            name: user.name,
                            email: user.email,
                            id: user.id,
                            provider: ntsSession.config.tokenKey,
                            imageUrl: user.imageUrl
                        });
                    }

                    setToken(null);
                    return $q.reject(null);
                },
                checkStatus: function () {
                    if (readToken())
                        return $q.when(1);
                    else
                        return $q.reject(1);
                },
                getToken: function () {
                    return getToken();
                },
                setToken: function (token) {
                    setToken(token);
                },
                initialize: function () {
                    return $q.when(1);
                }
            };
            $$sessionManagerDictionary[ntsSession.config.tokenKey] = media;
        }]);