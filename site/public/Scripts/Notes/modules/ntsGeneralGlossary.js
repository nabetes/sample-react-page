﻿angular.module("ntsGeneralGlossary", [])
    .factory("ntsGeneralGlossary", [function () {
        return {
            ntsSpinner: {
                tlpDelay: "Algo esta tardando mas de lo esperado...",
                tlpConnect: "Se ha perdido la conexion a internet"
            }
        };
    }]);