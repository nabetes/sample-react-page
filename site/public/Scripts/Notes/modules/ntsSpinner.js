﻿angular.module('ntsSpinner', [])
    .directive("ntsSpinnerButton", ["$$scopeProto",
        function ($$scopeProto) {
            let template = `<button data-html="true"
                                    data-toggle="tooltip"
                                    data-placement="left"
                                    class="nts-spinner btn btn--action btn--fixed" ng-class="::ntsBtnClass">
                                <div class="page-loader__spinner">
                                    <svg viewBox="25 25 50 50">
                                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                                    </svg>
                                </div>
                                <i  class="zmdi" ng-class="::ntsIcon"></i>
                            </button>`;

            return {
                restrict: 'A',
                scope: {
                    ntsSpinnerButton: "@",
                    ntsBtnClass: "=",
                    ntsIcon: "="
                },
                transclude: true,
                replace: true,
                template: template,
                link: function ($scope, $element, $attrs, $ctrl, $transclude) {
                    let $$directive = null,
                        progressBar = null;
                    $$scopeProto.extend($scope.$parent);

                    function $$controller() {
                        this._visible = false;
                        this.tooltip = $element.tooltip().data("bs.tooltip");
                        if ($attrs.ntsSpinnerButton) {
                            $scope.$parent.$$set($attrs.ntsSpinnerButton, this);
                        }
                    }

                    $$controller.prototype.show = function (tooltip) {
                        if (!this._visible) {
                            $element.attr("data-original-title", tooltip || null);                         
                            this._visible = true;
                            $element.fadeIn();
                        }
                    }

                    $$controller.prototype.hide = function () {
                        if (this._visible) {
                            this._visible = false;
                            $element.fadeOut();
                        }
                    }

                    return $$directive = new $$controller();
                }
            };
        }])
    .factory("ntsSpinner", [
        "$rootScope", "$document", "$compile",
        function ($rootScope, $document, $compile) {
            let delay = $rootScope.$new(),
                connect = $rootScope.$new();

            delay.sv = {
                spinner: null,
                btnClass: "btn-warning",
                icon: "zmdi-hourglass"
            };
            connect.sv = {
                spinner: null,
                btnClass: "btn-success",
                icon: "zmdi-input-power"
            };

            function spinner(scope) {
                this.scope = scope;
                this._element = null;
            }

            spinner.prototype.show = function (tooltip) {
                this.ready().show(tooltip);
            }

            spinner.prototype.hide = function () {
                if (this._element) {
                    this.scope.sv.spinner.hide();
                }
            }

            spinner.prototype.ready = function () {
                if (!this._element) {
                    this._element = $compile(`<button nts-spinner-button="sv.spinner"
                                                  nts-btn-class="::sv.btnClass"
                                                  nts-icon="::sv.icon"></button>`)(this.scope);
                    $document.find("body:first").prepend(this._element);
                }
                return this.scope.sv.spinner;
            }

            function ntsSpinner() {
                this.delay = new spinner(delay);
                this.connect = new spinner(connect);
            }

            return new ntsSpinner();
        }]);