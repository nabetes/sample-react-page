﻿window.profesor = {
    periodosYCursos: [
        {
            "cursoId": 23066,
            "periodoDisplayName": "Cartago - Periodo: 201901",
            "cursoDisplayName": "AN-BAN-01 - Administración General - Grupo: 5 Aula: CM-27 Día: J",
            "D": 0,
            "E": 22272,
            "horaInicio": "6:00PM",
            "horaSalida": "8:30PM",
            "H": 37
        },
        {
            "cursoId": 25060,
            "periodoDisplayName": "Cartago - Periodo: 201901",
            "cursoDisplayName": "AN-BAN-24 - Control Gerencial - Grupo: 1 Aula: CM-26 Día: L",
            "D": 0,
            "E": 22341,
            "horaInicio": "6:00PM",
            "horaSalida": "8:30PM",
            "H": 30
        }
    ],
    periodosYCursosDetalle: [
        {
            "periodoDisplayName": "Cartago - Periodo: 201901",
            "cursoDisplayName": "AN-BAN-01 - Administración General - Grupo: 5 Aula: CM-27 Día: J",
            "C": 0,
            "D": "J         ",
            "fechaInicio": "2019-01-01",
            "fechaFin": "2019-04-30",
            "aula": "CM-27",
            "H": 1,
            "I": 0,
            "K": 0,
            "L": 0,
            "cursoId": 23066,
            "N": 22272,
            "horaInicio": "6:00PM",
            "horaSalida": "8:30PM",
            "Q": "20190501210000",
            "R": "20190501203000"
        },
        {
            "periodoDisplayName": "Cartago - Periodo: 201901",
            "cursoDisplayName": "AN-BAN-24 - Control Gerencial - Grupo: 1 Aula: CM-26 Día: L",
            "C": 0,
            "D": "L         ",
            "fechaInicio": "2019-01-01",
            "fechaFin": "2019-04-30",
            "aula": "CM-26",
            "H": 1,
            "I": 0,
            "K": 0,
            "L": 0,
            "cursoId": 25060,
            "N": 22341,
            "horaInicio": "6:00PM",
            "horaSalida": "8:30PM",
            "Q": "20190501210000",
            "R": "20190501203000"
        }
    ]
};

window.studiantesConNota = [
    {
        "F": 0,
        "nose": 600668,
        "studentId": 84008,
        "id": "201910051640",
       "nombre": "ALFARO PEREIRA MARIA JOSE",
        "total": 83.0,
        "Asignaciones": 16.46,
        "Parciales": 47.0,
        "Pruebas_Cortas": 9.5,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600657,
        "studentId": 55395,
        "id": "201730050959",
       "nombre": "ALVARADO GOMEZ YASIN JOSE",
        "total": 90.0,
        "Asignaciones": 16.2,
        "Parciales": 54.8,
        "Pruebas_Cortas": 9.25,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600666,
        "studentId": 83648,
        "id": "201910051281",
       "nombre": "BEJARANO CAMPOS JUAN MANUEL",
        "total": 76.0,
        "Asignaciones": 16.46,
        "Parciales": 40.4,
        "Pruebas_Cortas": 8.75,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600654,
        "studentId": 83896,
        "id": "201910051535",
       "nombre": "BRENES GARITA CARLOS ENRIQUE",
        "total": 16.0,
        "Asignaciones": 11.7,
        "Parciales": 0.0,
        "Pruebas_Cortas": 4.0,
        "Trabajos_Investigación": 0.0
    },
    {
        "F": 0,
        "nose": 600669,
        "studentId": 80339,
        "id": "201820010941",
       "nombre": "BRENES GUZMAN JOHAN ANDRES",
        "total": 73.0,
        "Asignaciones": 8.5,
        "Parciales": 44.0,
        "Pruebas_Cortas": 10.0,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 608277,
        "studentId": 78311,
        "id": "201810051197",
       "nombre": "CALVO GRANADOS BRANDON ALBERTO",
        "total": 16.0,
        "Asignaciones": 3.76,
        "Parciales": 11.8,
        "Pruebas_Cortas": 0.0,
        "Trabajos_Investigación": 0.0
    },
    {
        "F": 0,
        "nose": 600643,
        "studentId": 83935,
        "id": "201910051570                  ",
       "nombre": "CHACON RODRIGUEZ KAREN DANIELA",
        "total": 89.0,
        "Asignaciones": 16.5,
        "Parciales": 53.8,
        "Pruebas_Cortas": 9.0,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600642,
        "studentId": 83846,
        "id": "201910051485",
       "nombre": "CHACON SOLIS JOSE DAVID",
        "total": 90.0,
        "Asignaciones": 16.5,
        "Parciales": 54.6,
        "Pruebas_Cortas": 9.25,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600673,
        "studentId": 84091,
        "id": "201910051729                  ",
       "nombre": "CUBERO SOLANO WILLIAM ALEXANDER",
        "total": 82.0,
        "Asignaciones": 16.5,
        "Parciales": 45.6,
        "Pruebas_Cortas": 9.5,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600659,
        "studentId": 16719,
        "id": "201410050358",
       "nombre": "DELGADO SANABRIA CRISTOPHER ALEXANDER",
        "total": 58.0,
        "Asignaciones": 16.26,
        "Parciales": 30.0,
        "Pruebas_Cortas": 5.0,
        "Trabajos_Investigación": 7.0
    },
    {
        "F": 0,
        "nose": 600640,
        "studentId": 82979,
        "id": "201910050601                  ",
       "nombre": "DURAN ALFARO LUIS ENRIQUE",
        "total": 72.0,
        "Asignaciones": 12.5,
        "Parciales": 44.2,
        "Pruebas_Cortas": 6.75,
        "Trabajos_Investigación": 9.0
    },
    {
        "F": 0,
        "nose": 600651,
        "studentId": 83757,
        "id": "201910051393",
       "nombre": "FONSECA CHACON YENDRY VALERIA",
        "total": 78.0,
        "Asignaciones": 12.0,
        "Parciales": 50.6,
        "Pruebas_Cortas": 6.75,
        "Trabajos_Investigación": 9.0
    },
    {
        "F": 0,
        "nose": 600641,
        "studentId": 83769,
        "id": "201910051402",
       "nombre": "GONZALEZ OBANDO SEBASTIAN",
        "total": 71.0,
        "Asignaciones": 12.0,
        "Parciales": 43.0,
        "Pruebas_Cortas": 6.75,
        "Trabajos_Investigación": 9.0
    },
    {
        "F": 0,
        "nose": 600639,
        "studentId": 82835,
        "id": "201910050456",
       "nombre": "HERNANDEZ ARCE BRAYAN ANDRES",
        "total": 70.0,
        "Asignaciones": 12.0,
        "Parciales": 42.4,
        "Pruebas_Cortas": 6.75,
        "Trabajos_Investigación": 9.0
    },
    {
        "F": 0,
        "nose": 600670,
        "studentId": 72465,
        "id": "201810050123",
       "nombre": "LOPEZ GUTIERREZ KATHERINE VANESSA",
        "total": 36.0,
        "Asignaciones": 8.5,
        "Parciales": 19.8,
        "Pruebas_Cortas": 7.5,
        "Trabajos_Investigación": 0.0
    },
    {
        "F": 0,
        "nose": 600661,
        "studentId": 79150,
        "id": "201810051174",
       "nombre": "MENESES VASQUEZ LISANDRO MANUEL",
        "total": 16.0,
        "Asignaciones": 6.2,
        "Parciales": 9.4,
        "Pruebas_Cortas": 0.0,
        "Trabajos_Investigación": 0.0
    },
    {
        "F": 0,
        "nose": 600655,
        "studentId": 84024,
        "id": "201910051659",
       "nombre": "MONGE NAVARRO YEILYN DE LOS ANGELES",
        "total": 48.0,
        "Asignaciones": 4.7,
        "Parciales": 28.6,
        "Pruebas_Cortas": 8.0,
        "Trabajos_Investigación": 7.0
    },
    {
        "F": 0,
        "nose": 600660,
        "studentId": 54884,
        "id": "201730050551",
       "nombre": "MONTOYA CORTES ROGER ALEXANDER",
        "total": 71.0,
        "Asignaciones": 15.2,
        "Parciales": 41.6,
        "Pruebas_Cortas": 7.0,
        "Trabajos_Investigación": 7.0
    },
    {
        "F": 0,
        "nose": 600663,
        "studentId": 83290,
        "id": "201910050902",
       "nombre": "MORA MENESES JESUS ANTONIO",
        "total": 74.0,
        "Asignaciones": 16.72,
        "Parciales": 39.8,
        "Pruebas_Cortas": 8.75,
        "Trabajos_Investigación": 9.0
    },
    {
        "F": 0,
        "nose": 600647,
        "studentId": 80447,
        "id": "201820051056",
       "nombre": "MORALES MATA STEPHANY VALERIA",
        "total": 13.0,
        "Asignaciones": 0.0,
        "Parciales": 13.0,
        "Pruebas_Cortas": 0.0,
        "Trabajos_Investigación": 0.0
    },
    {
        "F": 0,
        "nose": 600649,
        "studentId": 83428,
        "id": "201910051056                  ",
       "nombre": "OLIVARES ROBLES DIEGO ALEJANDRO",
        "total": 82.0,
        "Asignaciones": 12.5,
        "Parciales": 50.8,
        "Pruebas_Cortas": 9.0,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600671,
        "studentId": 82101,
        "id": "201830052518                  ",
       "nombre": "PACHECO JIMENEZ HELLEN ROXANA",
        "total": 82.0,
        "Asignaciones": 12.66,
        "Parciales": 50.0,
        "Pruebas_Cortas": 9.0,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600667,
        "studentId": 83787,
        "id": "201910051418",
       "nombre": "PIEDRA MENDEZ LUIS ENRIQUE",
        "total": 81.0,
        "Asignaciones": 17.16,
        "Parciales": 44.2,
        "Pruebas_Cortas": 9.5,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600656,
        "studentId": 84059,
        "id": "201910051693",
       "nombre": "QUESADA ARIAS MARISOL DE LOS ANGELES",
        "total": 92.0,
        "Asignaciones": 17.16,
        "Parciales": 55.0,
        "Pruebas_Cortas": 9.5,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600662,
        "studentId": 82998,
        "id": "201910050620",
       "nombre": "RAMIREZ BERMUDEZ DAYANA DE LOS ANGELES",
        "total": 73.0,
        "Asignaciones": 12.66,
        "Parciales": 41.0,
        "Pruebas_Cortas": 9.75,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600664,
        "studentId": 83294,
        "id": "201910050906",
       "nombre": "ROMAN ZUÑIGA JAVIER ALEJANDRO",
        "total": 53.0,
        "Asignaciones": 14.8,
        "Parciales": 20.0,
        "Pruebas_Cortas": 8.25,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600658,
        "studentId": 16583,
        "id": "201310050175",
       "nombre": "SANABRIA SOLANO JOSE DANIEL",
        "total": 74.0,
        "Asignaciones": 11.76,
        "Parciales": 45.8,
        "Pruebas_Cortas": 8.25,
        "Trabajos_Investigación": 8.0
    },
    {
        "F": 0,
        "nose": 600665,
        "studentId": 83381,
        "id": "201910051009",
       "nombre": "SANCHO CALVO ANTHONY ALEXIS",
        "total": 76.0,
        "Asignaciones": 15.3,
        "Parciales": 43.2,
        "Pruebas_Cortas": 7.5,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600672,
        "studentId": 84017,
        "id": "201910051654",
       "nombre": "SEGURA GAMBOA RAQUEL YULIANA",
        "total": 20.0,
        "Asignaciones": 11.8,
        "Parciales": 3.8,
        "Pruebas_Cortas": 4.25,
        "Trabajos_Investigación": 0.0
    },
    {
        "F": 0,
        "nose": 600644,
        "studentId": 84163,
        "id": "201910051829",
       "nombre": "SERRANO MUÑOZ KENNETH JOSE",
        "total": 76.0,
        "Asignaciones": 11.8,
        "Parciales": 49.6,
        "Pruebas_Cortas": 7.5,
        "Trabajos_Investigación": 7.0
    },
    {
        "F": 0,
        "nose": 600646,
        "studentId": 83675,
        "id": "201910051311",
       "nombre": "SILVA HERRERA GREIVIN ANTONIO",
        "total": 88.0,
        "Asignaciones": 16.5,
        "Parciales": 51.8,
        "Pruebas_Cortas": 9.5,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600650,
        "studentId": 83591,
        "id": "201910051224",
       "nombre": "SOLANO LEON NATALIA",
        "total": 79.0,
        "Asignaciones": 17.0,
        "Parciales": 43.2,
        "Pruebas_Cortas": 9.25,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600638,
        "studentId": 82795,
        "id": "201910050411",
       "nombre": "SOLANO LOPEZ ANDRES FRANCISCO",
        "total": 81.0,
        "Asignaciones": 16.5,
        "Parciales": 45.4,
        "Pruebas_Cortas": 9.25,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600648,
        "studentId": 82977,
        "id": "201910050599",
       "nombre": "TREJOS MORA DAYANNA DE LOS ANGELES",
        "total": 81.0,
        "Asignaciones": 16.5,
        "Parciales": 48.2,
        "Pruebas_Cortas": 9.0,
        "Trabajos_Investigación": 7.0
    },
    {
        "F": 0,
        "nose": 600653,
        "studentId": 83786,
        "id": "201910051422                ",
       "nombre": "VEGA BRENES MARIA DE LOS ANGELES",
        "total": 92.0,
        "Asignaciones": 18.0,
        "Parciales": 54.2,
        "Pruebas_Cortas": 9.5,
        "Trabajos_Investigación": 10.0
    },
    {
        "F": 0,
        "nose": 600652,
        "studentId": 83780,
        "id": "201910051411",
       "nombre": "VEGA RAMIREZ PAULA MARIA",
        "total": 88.0,
        "Asignaciones": 16.3,
        "Parciales": 55.4,
        "Pruebas_Cortas": 8.5,
        "Trabajos_Investigación": 7.3
    },
    {
        "F": 0,
        "nose": 600645,
        "studentId": 83067,
        "id": "201910050694",
       "nombre": "VEGA SANCHEZ KATHERINE FRANCINI",
        "total": 82.0,
        "Asignaciones": 16.3,
        "Parciales": 49.6,
        "Pruebas_Cortas": 8.75,
        "Trabajos_Investigación": 7.3
    }
];

window.studiantesConNotaDictionary = {};
window.studiantesConNota.forEach(function (item) {
    window.studiantesConNotaDictionary[item.id] = item;
})

//37
window.studiantesConCorreo = [
    {
        "A": 82795,
        "B": 600638,
        "id": "201910050411",
        "name": "ANDRES FRANCISCO",
        "firstLastName": "SOLANO",
        "secontLastName": "LOPEZ",
        "email": "asolano_07@hotmail.com",
        "H": 9,
        "I": "andres.solano2@uamcr.net"
    },
    {
        "A": 82835,
        "B": 600639,
        "id": "201910050456",
        "name": "BRAYAN ANDRES",
        "firstLastName": "HERNANDEZ",
        "secontLastName": "ARCE",
        "email": "hernandezbrayan1309@gmail.com",
        "H": 9,
        "I": "brayan.hernandez@uamcr.net"
    },
    {
        "A": 82979,
        "B": 600640,
        "id": "201910050601                  ",
        "name": "LUIS ENRIQUE",
        "firstLastName": "DURAN",
        "secontLastName": "ALFARO",
        "email": "duranalfaro120@gmail.com",
        "H": 9,
        "I": "luis.duran1@uamcr.net"
    },
    {
        "A": 83769,
        "B": 600641,
        "id": "201910051402",
        "name": "SEBASTIAN",
        "firstLastName": "GONZALEZ",
        "secontLastName": "OBANDO",
        "email": "sebasgo_16@hotmail.com",
        "H": 9,
        "I": "sebastian.gonzalez1@uamcr.net"
    },
    {
        "A": 83846,
        "B": 600642,
        "id": "201910051485",
        "name": "JOSE DAVID",
        "firstLastName": "CHACON",
        "secontLastName": "SOLIS",
        "email": "jdchacon@earth.ac.cr",
        "H": 9,
        "I": "jose.chacon8@uamcr.net"
    },
    {
        "A": 83935,
        "B": 600643,
        "id": "201910051570                  ",
        "name": "KAREN DANIELA",
        "firstLastName": "CHACON",
        "secontLastName": "RODRIGUEZ",
        "email": "karendanielaro10@gmail.com",
        "H": 9,
        "I": "karen.chacon@uamcr.net"
    },
    {
        "A": 84163,
        "B": 600644,
        "id": "201910051829",
        "name": "KENNETH JOSE",
        "firstLastName": "SERRANO",
        "secontLastName": "MUÑOZ",
        "email": "kennethserrano04@gmail.com",
        "H": 9,
        "I": "kenneth.serrano1@uamcr.net"
    },
    {
        "A": 83067,
        "B": 600645,
        "id": "201910050694",
        "name": "KATHERINE FRANCINI",
        "firstLastName": "VEGA",
        "secontLastName": "SANCHEZ",
        "email": "katherinevega70@hotmail.com",
        "H": 9,
        "I": "katherine.vega2@uamcr.net"
    },
    {
        "A": 83675,
        "B": 600646,
        "id": "201910051311",
        "name": "GREIVIN ANTONIO",
        "firstLastName": "SILVA",
        "secontLastName": "HERRERA",
        "email": "silva.greivin@gmail.com",
        "H": 9,
        "I": "greivin.silva@uamcr.net"
    },
    {
        "A": 82977,
        "B": 600648,
        "id": "201910050599",
        "name": "DAYANNA DE LOS ANGELES",
        "firstLastName": "TREJOS",
        "secontLastName": "MORA",
        "email": "dayiitrejosm08@gmail.com",
        "H": 9,
        "I": "dayanna.trejos@uamcr.net"
    },
    {
        "A": 83428,
        "B": 600649,
        "id": "201910051056                  ",
        "name": "DIEGO ALEJANDRO",
        "firstLastName": "OLIVARES",
        "secontLastName": "ROBLES",
        "email": "diegoolivares208@gmail.com",
        "H": 9,
        "I": "diego.olivares@uamcr.net"
    },
    {
        "A": 83591,
        "B": 600650,
        "id": "201910051224",
        "name": "NATALIA",
        "firstLastName": "SOLANO",
        "secontLastName": "LEON",
        "email": "nasole_11@hotmail.com",
        "H": 9,
        "I": "natalia.solano2@uamcr.net"
    },
    {
        "A": 83757,
        "B": 600651,
        "id": "201910051393",
        "name": "YENDRY VALERIA",
        "firstLastName": "FONSECA",
        "secontLastName": "CHACON",
        "email": "yendryfonseca4@gmail.com",
        "H": 9,
        "I": "yendry.fonseca2@uamcr.net"
    },
    {
        "A": 83780,
        "B": 600652,
        "id": "201910051411",
        "name": "PAULA MARIA",
        "firstLastName": "VEGA",
        "secontLastName": "RAMIREZ",
        "email": "paula@tecagrisa.com",
        "H": 9,
        "I": "paula.vega1@uamcr.net"
    },
    {
        "A": 83786,
        "B": 600653,
        "id": "201910051422                ",
        "name": "MARIA DE LOS ANGELES",
        "firstLastName": "VEGA",
        "secontLastName": "BRENES",
        "email": "marielosvegabrenes1094@gmail.com",
        "H": 9,
        "I": "maria.vega8@uamcr.net"
    },
    {
        "A": 84059,
        "B": 600656,
        "id": "201910051693",
        "name": "MARISOL DE LOS ANGELES",
        "firstLastName": "QUESADA",
        "secontLastName": "ARIAS",
        "email": "mquesada050@gmail.com",
        "H": 9,
        "I": "marisol.quesada1@uamcr.net"
    },
    {
        "A": 55395,
        "B": 600657,
        "id": "201730050959",
        "name": "YASIN JOSE",
        "firstLastName": "ALVARADO",
        "secontLastName": "GOMEZ",
        "email": "yashinj.91@gmail.com",
        "H": 9,
        "I": "yasin.alvarado@uamcr.net"
    },
    {
        "A": 16583,
        "B": 600658,
        "id": "201310050175",
        "name": "JOSE DANIEL",
        "firstLastName": "SANABRIA",
        "secontLastName": "SOLANO",
        "email": "danielavanged10@gmail.com",
        "H": 9,
        "I": "jose.sanabria@uamcr.net"
    },
    {
        "A": 54884,
        "B": 600660,
        "id": "201730050551",
        "name": "ROGER ALEXANDER",
        "firstLastName": "MONTOYA",
        "secontLastName": "CORTES",
        "email": "alexandermc181@hotmail.com",
        "H": 9,
        "I": "roger.montoya@uamcr.net"
    },
    {
        "A": 82998,
        "B": 600662,
        "id": "201910050620",
        "name": "DAYANA DE LOS ANGELES",
        "firstLastName": "RAMIREZ",
        "secontLastName": "BERMUDEZ",
        "email": "dbermudezcartago@hotmail.com",
        "H": 9,
        "I": "dayana.ramirez@uamcr.net"
    },
    {
        "A": 83290,
        "B": 600663,
        "id": "201910050902",
        "name": "JESUS ANTONIO",
        "firstLastName": "MORA",
        "secontLastName": "MENESES",
        "email": "jtutu2011@gmail.com",
        "H": 9,
        "I": "jesus.mora2@uamcr.net"
    },
    {
        "A": 83381,
        "B": 600665,
        "id": "201910051009",
        "name": "ANTHONY ALEXIS",
        "firstLastName": "SANCHO",
        "secontLastName": "CALVO",
        "email": "sanchoanthony37@gmail.com",
        "H": 9,
        "I": "anthony.sancho@uamcr.net"
    },
    {
        "A": 83648,
        "B": 600666,
        "id": "201910051281",
        "name": "JUAN MANUEL",
        "firstLastName": "BEJARANO",
        "secontLastName": "CAMPOS",
        "email": "bejaranoj30@gmail.com",
        "H": 9,
        "I": "juan.bejarano1@uamcr.net"
    },
    {
        "A": 83787,
        "B": 600667,
        "id": "201910051418",
        "name": "LUIS ENRIQUE",
        "firstLastName": "PIEDRA",
        "secontLastName": "MENDEZ",
        "email": "luismndez95@gmail.com",
        "H": 9,
        "I": "luis.piedra3@uamcr.net"
    },
    {
        "A": 84008,
        "B": 600668,
        "id": "201910051640",
        "name": "MARIA JOSE",
        "firstLastName": "ALFARO",
        "secontLastName": "PEREIRA",
        "email": "malfaro2612@hotmail.com",
        "H": 9,
        "I": "maria.alfaro13@uamcr.net"
    },
    {
        "A": 80339,
        "B": 600669,
        "id": "201820010941",
        "name": "JOHAN ANDRES",
        "firstLastName": "BRENES",
        "secontLastName": "GUZMAN",
        "email": "johanb1203@gmail.com",
        "H": 9,
        "I": "johan.brenes2@uamcr.net"
    },
    {
        "A": 82101,
        "B": 600671,
        "id": "201830052518                  ",
        "name": "HELLEN ROXANA",
        "firstLastName": "PACHECO",
        "secontLastName": "JIMENEZ",
        "email": "hellenpa97@gmail.com",
        "H": 9,
        "I": "hellen.pacheco@uamcr.net"
    },
    {
        "A": 84091,
        "B": 600673,
        "id": "201910051729                  ",
        "name": "WILLIAM ALEXANDER",
        "firstLastName": "CUBERO",
        "secontLastName": "SOLANO",
        "email": "williamcuberosolano@gmail.com",
        "H": 9,
        "I": "william.cubero@uamcr.net"
    },
    {
        "A": 80447,
        "B": 600647,
        "id": "201820051056",
        "name": "STEPHANY VALERIA",
        "firstLastName": "MORALES",
        "secontLastName": "MATA",
        "email": "nanymm96@hotmail.com",
        "H": 9,
        "I": "stephany.morales@uamcr.net"
    },
    {
        "A": 83896,
        "B": 600654,
        "id": "201910051535",
        "name": "CARLOS ENRIQUE",
        "firstLastName": "BRENES",
        "secontLastName": "GARITA",
        "email": "klobg03@gmail.com",
        "H": 9,
        "I": "carlos.brenes4@uamcr.net"
    },
    {
        "A": 84024,
        "B": 600655,
        "id": "201910051659",
        "name": "YEILYN DE LOS ANGELES",
        "firstLastName": "MONGE",
        "secontLastName": "NAVARRO",
        "email": "ymonge@coopedota.com",
        "H": 9,
        "I": "yeilyn.monge@uamcr.net"
    },
    {
        "A": 16719,
        "B": 600659,
        "id": "201410050358",
        "name": "CRISTOPHER ALEXANDER",
        "firstLastName": "DELGADO",
        "secontLastName": "SANABRIA",
        "email": "delgacris123@outlook.com",
        "H": 9,
        "I": "cristopher.delgado1@uamcr.net"
    },
    {
        "A": 79150,
        "B": 600661,
        "id": "201810051174",
        "name": "LISANDRO MANUEL",
        "firstLastName": "MENESES",
        "secontLastName": "VASQUEZ",
        "email": "manumenevas17@gmail.com",
        "H": 9,
        "I": "lisandro.meneses@uamcr.net"
    },
    {
        "A": 83294,
        "B": 600664,
        "id": "201910050906",
        "name": "JAVIER ALEJANDRO",
        "firstLastName": "ROMAN",
        "secontLastName": "ZUÑIGA",
        "email": "javieromanz@hotmail.com",
        "H": 9,
        "I": "javier.roman@uamcr.net"
    },
    {
        "A": 72465,
        "B": 600670,
        "id": "201810050123",
        "name": "KATHERINE VANESSA",
        "firstLastName": "LOPEZ",
        "secontLastName": "GUTIERREZ",
        "email": "kathie.gz9306@gmail.com",
        "H": 9,
        "I": "katherine.lopez@uamcr.net"
    },
    {
        "A": 84017,
        "B": 600672,
        "id": "201910051654",
        "name": "RAQUEL YULIANA",
        "firstLastName": "SEGURA",
        "secontLastName": "GAMBOA",
        "email": "rakesegura01@gmail.com",
        "H": 9,
        "I": "raquel.segura1@uamcr.net"
    },
    {
        "A": 78311,
        "B": 608277,
        "id": "201810051197",
        "name": "BRANDON ALBERTO",
        "firstLastName": "CALVO",
        "secontLastName": "GRANADOS",
        "email": "brandongranados94@gmail.com",
        "H": 9,
        "I": "brandon.calvo@uamcr.net"
    }
];

window.cuadroEvaluacionColoresRubro = {
    Parciales: 'bg-teal',
    Pruebas_Cortas: 'bg-blue',
    Trabajos_Investigación: 'bg-green',
    Asignaciones: 'bg-amber',
    "bg-cyan": 0
};

   

window.cuadroEvaluacion = [
    {
        "RubroEvaluacionId": 104235,
        "B": 1,
        "cuadroevaluacionId": 14807,
        "fechaEvaluacion": "2019/02/14",
        "porcentaje": 20.0,
        "rubroDisplay": "Parciales",
        "rubroName": "Parciales"
    },
    {
        "RubroEvaluacionId": 104236,
        "B": 1,
        "cuadroevaluacionId": 14807,
        "fechaEvaluacion": "2019/03/21",
        "porcentaje": 20.0,
        "rubroDisplay": "Parciales #2",
        "rubroName": "Parciales"
    },
    {
        "RubroEvaluacionId": 104237,
        "B": 1,
        "cuadroevaluacionId": 14807,
        "fechaEvaluacion": "2019/04/25",
        "porcentaje": 20.0,
        "rubroDisplay": "Parciales #3",
        "rubroName": "Parciales"
    },
    {
        "RubroEvaluacionId": 104238,
        "B": 2,
        "cuadroevaluacionId": 14807,
        "fechaEvaluacion": "2019/02/07",
        "porcentaje": 5.0,
        "rubroDisplay": "Pruebas_Cortas",
        "rubroName": "Pruebas_Cortas"
    },
    {
        "RubroEvaluacionId": 104239,
        "B": 2,
        "cuadroevaluacionId": 14807,
        "fechaEvaluacion": "2019/04/25",
        "porcentaje": 5.0,
        "rubroDisplay": "Pruebas_Cortas #2",
        "rubroName": "Pruebas_Cortas"
    },
    {
        "RubroEvaluacionId": 104240,
        "B": 5,
        "cuadroevaluacionId": 14807,
        "fechaEvaluacion": "2019/03/28",
        "porcentaje": 10.0,
        "rubroDisplay": "Trabajos_Investigación",
        "rubroName": "Trabajos_Investigación"
    },
    {
        "RubroEvaluacionId": 104241,
        "B": 15,
        "cuadroevaluacionId": 14807,
        "fechaEvaluacion": "2019/04/25",
        "porcentaje": 20.0,
        "rubroDisplay": "Asignaciones",
        "rubroName": "Asignaciones"
    }
];