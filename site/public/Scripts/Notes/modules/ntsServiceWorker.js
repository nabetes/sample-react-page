﻿angular.module("ntsServiceWorker", ["ntsSpinner", "$$exception", "ntsURL"])
    .factory("ntsServiceWorker", ["$q", "ntsSpinner", "$$exception", "ntsURL",
        function ($q, ntsSpinner, $$exception, ntsURL) {
            function ntsServiceWorker() {
                this.enable = 'serviceWorker' in navigator;

                if (!this.enable)
                    return;

                navigator.serviceWorker.getRegistration()
                    .then(function (registrations) {
                        /*Just could be one worker per domain*/
                        if (!registrations) {
                            navigator.serviceWorker.register(ntsURL.ntsServiceWorker.file, {
                                scope: '/'
                            }).then(function (reg) {
                                console.log('ntsServiceWorker Registration succeeded. Scope is ' + reg.scope);
                            }, function () {
                                console.error('ntsServiceWorker Registration failed with ' + error);
                            });
                        }
                    });

                navigator.serviceWorker.addEventListener('message', function (event) {
                    let code = event.data.code;
                    switch (code) {
                        case "reconnected":
                            ntsSpinner.connect.hide();
                            break;
                        case "cache":
                            event.data.code = "unhandled error";
                            let error = new $$exception(event.data);
                            ntsSpinner.connect.show(error.getHtml());
                            break;
                    }
                });
            }

            return new ntsServiceWorker;
        }]);