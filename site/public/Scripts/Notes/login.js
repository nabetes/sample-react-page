﻿'use strict';
angular.module("loginApp", ['ntsConfig', 'ntsLoginGlossary'])
    .controller("loginController", ["$scope", "$window",
        "$$session", "$$exception", "$http", "$$message",
        "ntsSession", "ntsSessionModel", "ntsLoginGlossary", "ntsURL",
        function ($scope, $window,
            $$session, $$exception, $$http, $$message,
            ntsSession, ntsSessionModel, ntsGlossary, ntsURL) {
            let $$this = new $$controller();

            function $$controller() {
                $scope.frmLogin = {};
                $scope.frmRegister = {};
                $scope.frmForgot = {};
                $scope.sv = {
                    socialMediaButtons: [
                        {
                            'class': 'icon-facebook',
                            key: "facebook"
                        },
                        {
                            'class': 'icon-google-plus',
                            key: "google"
                        },
                        //{
                        //    'class': 'icon-twitter'
                        //}
                    ],
                    user: {
                        user: null,
                        pass: null
                    },
                    register: {
                        name: null,
                        user: null,
                        pass: null,
                        license: false
                    },
                    forgot: {
                        user: null
                    },
                    emailPattern: /^\S+@\S+$/
                };

                $scope.login = function (btn) {
                    $$this.login(btn);
                };

                $scope.ntsLogin = function () {
                    $$this.ntsLogin();
                };

                $scope.ntsRegister = function () {
                    $$this.ntsRegister();
                };

                $scope.ntsForgot = function () {
                    $$this.ntsForgot(btn);
                };
            }

            $$controller.prototype.login = function (btn) {
                if (!$$http.$$requests.length) {
                    $$session.login(btn.key).then(function () {
                        $$message.notify({
                            onClose: function () {
                                $window.location.href = window.location.origin;
                            },
                            type: "success",
                            delay: 400
                        });
                    }, function (ex) {
                        $scope.frmLogin.$$form.inputsAddClass("has-danger");
                        throw ex;
                    });
                }
            }

            $$controller.prototype.ntsLogin = function () {
                ntsSessionModel.set($scope.sv.user);
                $scope.login({
                    key: ntsSession.config.tokenKey
                });
            }

            $$controller.prototype.ntsRegister = function () {
                if (!$$http.$$requests.length) {
                    $$http.post(ntsURL.login.register, $scope.sv.register)
                        .then(function (request) {
                            $$session.setToken(request.data.Data);
                            $$message.notify({
                                onClose: function () {
                                    $window.location.href = window.location.origin;
                                },
                                type: "success",
                                message: ntsGlossary.login.ntRegisterSuccess,
                                delay: 1000
                            });
                        }, function (ex) {
                            $scope.frmRegister.$$form.inputsAddClass("has-danger");
                            throw ex;
                        });
                }
            }

            $$controller.prototype.ntsForgot = function () {
                if (!$$http.$$requests.length) {
                    $$http.post(ntsURL.login.forgotPassword, $scope.sv.forgot)
                        .then(function () {
                            $$message.notify({
                                type: "success",
                                message: ntsGlossary.login.ntsRecoverPasswordSuccess.$$stringFormat($scope.sv.forgot.user),
                                delay: 10000
                            });
                        }, function (ex) {
                            $scope.frmForgot.$$form.inputsAddClass("has-danger");
                            throw ex;
                        });
                }
            };   
            
            angular.element(document).ready(function () {
                $('.page-loader').fadeOut();
            });
        }]);    