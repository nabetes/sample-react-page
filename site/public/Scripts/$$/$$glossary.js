﻿angular.module("$$glossary", [])
    .factory("$$glossary", [function () {
        return {
            //'$$': {},
            '$$exception': {
                msgGenericMessage: "Ocurrió algo inesperado, comunique con el encargado de soporte.",
                btnAccept: "Aceptar",
                mdTitle: "Atencíon",
            },
            '$$http': {},
            '$$modal': {},
            '$$popupBlockerCheck': {
                msgPopUpIsBlock: "El navegador esta realizando un bloqueo activo de ventanas emergentes."
            },
            '$$queue': {},
            '$$session': {},
            '$$sessionFacebook': {
                msgFail: "No se realizó el inicio sesión con Facebook"
            },
            '$$sessionGoogle': {
                msgFail: "No se realizó el inicio sesión con google"
            },
            '$$state': {},
            '$$worker': {},
            '$$form': {
                mgsInputDoesntExist: '<b>$$validate</b> directive input <b>{0}</b> doesnt exists',
                validators: {
                    required: "El valor del campo <b>{0}</b> es requerido.",
                    pattern: "El valor del campo <b>{0}</b> no cumple el formato esperado.",
                    minlength: "La extensión del campo <b>{0}</b> no completa el mínimo de caracteres esperado. ",
                    maxlength: "La extensión del campo <b>{0}</b> sobrepasa el máximo de caracteres esperado. ",
                    min: "El valor del campo <b>{0}</b> no completa  el mínimo valor esperado. ",
                    max: "El valor del campo <b>{0}</b> sobrepasa el máximo valor esperado. ",
                }
            },
            '$$message': {
                mdTitleAlert: "Atencíon",
                mdTitleCofirm: "Atencíon",
                btnOkAlert: "Aceptar",
                btnOkConfirm: "Aceptar",
                btnCancelConfirm: "Cancelar",
                notification: {
                    "warning": {
                        title: "Atencíon!",
                        message: "...",
                    },
                    "success": {
                        title: "Mensaje del sistema:",
                        message: "Operación procesada de manera exitosa.",
                    },
                    "danger": {
                        title: "Ups!",
                        message: "Sucedió algo inesperado.",
                    },
                    "info": {
                        title: "Mensaje del sistema:",
                        message: "...",
                    }
                }
            },
            '$$paginator': {
                msgQueryNoResults: 'La consulta no retorno resultados…',
                msgQueryResults: 'Mostrando {0} - {1} de {2} registros'
            }
        };
    }]);