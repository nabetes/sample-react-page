﻿angular.module("$$backgroundFilter", ['$$worker'])
    .factory("$$backgroundFilter", ["$$worker", function ($$worker) {
        function $$backgroundFilter(config) {
            config = Object.assign({
                name: "backgroundFilter",
                $$map: ""
            }, config);
            this.worker = new $$worker({
                name: config.name,
                content: [config.$$map, function () {
                    function checkObject(item, filter) {
                        let flag = false,
                            itemKeys = Object.keys(item);

                        for (let index = 0, length = itemKeys.length, key, prop; index < length; index++) {
                            key = itemKeys[index];
                            prop = item[key] || -1;
                            if (typeof prop !== "string" && Object.keys(prop).length)
                                flag = checkObject(prop, filter);
                            else if (prop.toString().replace(/(\r\n|\n|\r)/gm, "").trim().toLowerCase().indexOf(filter) !== -1)
                                flag = true;

                            if (flag)
                                break;
                        }

                        return flag;
                    }
                    function $$do(data) {
                        let $this = this,
                            collection = data.collection,
                            length = collection.length,
                            filter = (data.filter || "").trim().toLowerCase(),
                            reportProgress = data.reportProgress,
                            progressPerItem = length && reportProgress ? 100 / length : 0,
                            result = [];

                        if (filter === "")
                            result = collection;
                        else {
                            for (let index = 0, item, currentProgress = 0; index < length; index++) {
                                if (reportProgress)
                                    $this.$$sendProgress(currentProgress += progressPerItem);
                                item = collection[index];
                                if ($this.$$map)
                                    item = $this.$$map(item);
                                if (checkObject(item, filter))
                                    result.push(item);
                            }
                        }
                        this.$$send(result);
                    }
                }]
            });
        }

        $$backgroundFilter.prototype.dispose = function () {
            this.worker.dispose();
        }

        $$backgroundFilter.prototype.filter = function (collection, filter, onProgress) {
            return this.worker.send({
                id: "$$backgroundFilter",
                collection: collection,
                filter: filter,
                reportProgress: !!onProgress
            }, onProgress).promise;
        }

        return $$backgroundFilter;
    }]);