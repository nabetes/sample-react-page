﻿'use strict';
angular.module('$$http', [])
    .provider("$$http", ["$httpProvider", function ($httpProvider) {
        let $$this = this;
        $$this.$$config = {
            onStard: null, //$delegate, url, data, config,
            onFinish: null
        };

        $$this.$$setConfig = function (config) {
            Object.assign($$this.$$config, config);
        }

        $$this.$get = function () {
            function $$http() {
                this.__requests = {};
                this.$$requests = [];
                $.extend(this, $$http.prototype);
            }

            $$http.prototype.$$setConfig = function (config) {
                $$this.$$setConfig(config);
            }

            $$http.prototype.$$getHttpProvider = function (config) {
                return $httpProvider;
            }

            $$http.prototype.$$execute = function (url, data, config, callback) {
                let request,
                    $this = this,                    
                    requestId = new Date().valueOf();

                if ($$this.$$config.onStard)
                    request = $$this.$$config.onStard(function () {
                        return callback.call($this, url, data, config)
                            .finally(function () {
                                $this._$$removeRequestFromQueue(requestId);
                                if (!$this.$$requests.length && $$this.$$config.onFinish)
                                    $$this.$$config.onFinish(data);
                            });
                    }, url, data, config);
                else {
                    request = callback.call($this, url, data, config);
                }
                this._$$addRequestToQueue(requestId, request);
                return request;
            }

            $$http.prototype._$$addRequestToQueue = function (id, request) {
                this.$$requests.push((this.__requests[id] = request));
            }

            $$http.prototype._$$removeRequestFromQueue = function (id) {
                let $this = this;
                delete this.__requests[id];
                this.$$requests = Object.keys(this.__requests).map(function (key) {
                    return $this.__requests[key];
                });
            }

            $$http.prototype.get = function () {
                throw "No Implemented";
            }

            $$http.prototype.post = function () {
                throw "No Implemented";
            }

            return $$http;
        };
    }])
    .config(["$provide", function ($provide) {
        $provide.decorator('$http',
            ["$delegate", "$injector",
                function ($delegate, $injector) {
                    let $$http = $injector.get("$$http"),
                        $get = $delegate.get,
                        $post = $delegate.post;

                    /*extends $$ prototype*/
                    $$http.call($delegate);

                    $delegate.post = function (url, data, config) {
                        return $delegate.$$execute(url, data, config, $post);
                    };

                    $delegate.get = function (url, data, config) {
                        return $delegate.$$execute(url, data, config, $get);
                    };

                    return $delegate;
                }]);
    }]);