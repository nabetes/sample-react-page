﻿'use strict';
angular.module('$$exception', ["$$glossary", "$$modal"])
    .provider("$$exception", function () {
        var $$this = this;
        $$this.$$config = {
            /*
            * Execute when an exception appear
            */
            onError: null,
            showTrace: false
        };

        $$this.$$setConfig = function (config) {
            Object.assign($$this.$$config, config);
        }

        $$this.$get = ["$rootScope", "$document", "$compile", "$$glossary",
            function ($rootScope, $document, $compile, $$glossary) {
                let modalTemplate = `<div id="SSexception-modal"
                                     $$modal="sv.modal"
                                     $$on-close="sv.onClose"
                                     $$title="sv.mdTitle">
                                    <div class="modal-body SSexception-modal-list"
                                         ng-bind-html="sv.msgExceptions">
                                    </div>
                                    <footer class="modal-footer">
                                        <button type="button"
                                                class="btn btn-primary waves-effect"
                                                data-dismiss="modal"
                                                ng-bind="sv.btnAccept"></button>
                                    </footer>
                                </div>`,
                    $scope = null;
                $scope = $rootScope.$new();
                $scope.sv = {
                    msgExceptions: "",
                    btnAccept: $$glossary.$$exception.btnAccept,
                    mdTitle: $$glossary.$$exception.mdTitle,
                    modal: null,
                    onClose: null
                };

                /**
                * Constructor of the class
                * @param {object} error: error
                */
                function $$exception() {
                    this.Exceptions = [];
                    this.code = "handled error";
                    this.innerException = null;
                    this.stack = null;
                    if (arguments.length) {
                        $$exception.prototype.add.apply(this, $$.toArray(arguments));
                    }
                };

                $$exception.$$setConfig = function (config) {
                    $$this.$$setConfig(config);
                }

                /**
                * Add new exceptions to the colecction
                * @param {object} error: error
                * @returns {$$exception} this
                */
                $$exception.prototype.add = function () {
                    var $this = this,
                        error = arguments[0];
                    switch (typeof error) {
                        case "string":
                            if (arguments.length > 1) {
                                Array.prototype.shift.apply(arguments);
                                error = String.prototype.$$stringFormat.apply(error, $$.toArray(arguments));
                            }
                            $this.addStringError(error);
                            break;
                        case "object":
                            if (error.constructor === $$exception) {
                                $this.innerException = error.innerException;
                                $this.Exceptions = $this.Exceptions.concat(error.Exceptions);
                            }
                            else if (error.Exceptions && $.isArray(error.Exceptions)) {
                                $this.code = error.code || "unhandled error";
                                $this.innerException = error.innerException;
                                if (error.code !== "handled error") {
                                    for (let index = 0, length = error.Exceptions.length; index < length; index++) {
                                        $this.addStringError(error.Exceptions[index]);
                                    }
                                    error.code === "handled error"
                                } else {
                                    $this.Exceptions = $this.Exceptions.concat(error.Exceptions);
                                }
                            }
                            else {
                                $this.innerException = error.innerException || error;
                                $this.addStringError($$glossary.$$exception.msgGenericMessage);
                            }
                            break;
                    }
                    return this;
                };

                /**
                 * Add a string exception
                 * @param {object} error: error
                 * @returns {$$exception} $$exception
                 */
                $$exception.prototype.addStringError = function (error) {
                    this.Exceptions.push('<span class="SSexception-item"> <span class="SSexception-item-icon zmdi zmdi-alert-circle zmdi-hc-fw"></span>{0}</span>'.$$stringFormat(error));
                    return this;
                }

                /**
                * Check if the handler have exceptions
                */
                $$exception.prototype.validate = function () {
                    if (this.Exceptions.length) {
                        this.throw();
                    }
                };

                /**
                * Add and throw a new exception
                * @param {object} error: error
                */
                $$exception.prototype.addAndThrow = function () {
                    $$exception.prototype.add.apply(this, $$.toArray(arguments));
                    this.throw();
                };

                /**
                * Clean the exceptions
                */
                $$exception.prototype.clean = function () {
                    this.Exceptions = [];
                };

                /**
                 * throw the current handler
                 */
                $$exception.prototype.throw = function () {
                    throw this;
                };

                /**
                 * Gets a string with the html of the exceptions
                 * @returns {string} message: html content
                 */
                $$exception.prototype.getHtml = function () {
                    return this.Exceptions.join("");
                };

                /* Show an exception
                * @param {generic} exception 
                */
                $$exception.prototype.show = function () {
                    console.trace();
                    if (!$scope.sv.modal)
                        $document.find("body:first").append($compile(modalTemplate)($scope));

                    if (arguments.length)
                        $$exception.prototype.add.apply(this, $$.toArray(arguments));

                    $scope.sv.msgExceptions = this.getHtml();
                    this.Exceptions = [];

                    if (!$scope.$root.$$phase)
                        $scope.$root.$digest();

                    $scope.sv.modal.open();
                    return $scope.sv.modal;
                }

                /**
                 * Add and throw
                 * @param {object} parametros del error
                 */
                $$exception.addAndThrow = function () {
                    $$exception.prototype.addAndThrow.apply(new $$exception(), arguments);
                };

                /* Show an exception
                * @param {generic} exception 
                */
                $$exception.show = function () {
                    var exception = arguments[0];
                    if (exception.constructor !== $$exception) {
                        if (arguments.length > 1) {
                            exception = new $$exception();
                            exception.add.apply(exception, arguments);
                        } else {
                            exception = new $$exception(exception);
                        }
                    }
                    return exception.show();
                };

                $$exception.$$scope = function () {
                    return $scope;
                }

                return $$exception;
            }];
    })
    .config(["$provide", "$$exceptionProvider", function ($provide, $$exceptionProvider) {
        $provide.decorator('$exceptionHandler', ["$delegate", "$injector", function ($delegate, $injector) {
            let $$exception;
            return function (error, cause) {
                if (!$$exception)
                    $$exception = $injector.get("$$exception");

                if (error.constructor !== $$exception)
                    $delegate(error, cause);//default error handling         

                if ($$exceptionProvider.$$config.onError)
                    return $$exceptionProvider.$$config.onError(error, cause);//custom error

                $$exception.show(error);
            };
        }]);
    }]);