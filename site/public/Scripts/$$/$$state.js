﻿'use strict';
angular.module('$$state', ['ui.router', 'oc.lazyLoad'])
    .provider('$$stateDictionary', function () {
        this.$get = function () {
            return {
                scripts: {}
            };
        };
    })
    .provider('$$state2', ["$stateProvider", "$urlRouterProvider",
        function ($stateProvider, $urlRouterProvider) {
            var $$this = this;
            $$this.$$config = {
                onChange: null // to, params, options, $delegate
            };

            $$this.$$setConfig = function (config) {
                Object.assign($$this.$$config, config);
            }

            $$this.$get = ['$ocLazyLoad', '$q', '$rootScope',
                "$$stateDictionary",
                function ($ocLazyLoad, $q, $rootScope,
                    $$stateDictionary) {

                    function $$state() {  
                        let $this = this;
                        $this._$$dispose = null;
                        $.extend($this, $$state.prototype);

                        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                            if ($$this.$$config.onChange)
                                $$this.$$config.onChange(event, toState, toParams, fromState, fromParams);
                            
                            if ($this._$$dispose) {
                                $this._$$dispose(event, toState, toParams, fromState, fromParams);
                                $this._$$dispose = null;
                            };
                        });
                    }

                    $$state.prototype.$$resolve = function () {
                        let $arguments = arguments;
                        return {
                            _modulesAndScripts: function () {
                                var requestsCollection = [];
                                for (var index = 0, length = $arguments.length, config = null, request = null; index < length; index++) {
                                    config = $arguments[index];
                                    if (typeof config === "string") {
                                        request = $ocLazyLoad.load($$stateDictionary.scripts[config]);
                                    } else {
                                        request = $ocLazyLoad.load(config);
                                    }
                                    requestsCollection.push(request);
                                }
                                return $q.all(requestsCollection);
                            }
                        };
                    }

                    $$state.prototype.$$otherwise = function () {
                        return $urlRouterProvider.otherwise(name);
                    }

                    $$state.prototype.$$state = function (name, config) {
                        return $stateProvider.state(name, config);
                    }

                    $$state.prototype.$$setConfig = function (config) {
                        $$this.$$setConfig(config);
                    }

                    $$state.prototype.$$dispose = function (callback) {
                        this._$$dispose = callback;
                    }

                    return $$state;
                }];
        }])
    .config(["$provide", "$$state2Provider", function ($provide, $$state2Provider) {
        $provide.decorator('$state',
            ["$delegate", "$injector",
                function ($delegate, $injector) {
                    let $$state = $injector.get("$$state2");
                    $delegate.$baseGo = $delegate.go;

                    /*extends $$ prototype*/
                    $$state.call($delegate);
                    
                    return $delegate;
                }]);
    }]);