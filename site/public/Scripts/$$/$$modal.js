﻿angular.module('$$modal', ["$$scopeProto"])
    .directive("$$modal", ["$$scopeProto",
        function ($$scopeProto) {
            let template = `<div class="modal fade"
                                 tabindex="-1"
                                 style="margin-right:10px">
                                <div class="modal-dialog" ng-class="::'modal-'+$$size">
                                    <div class="modal-content">
                                        <ng-transclude ng-if="::$$header"
                                                       ng-transclude-slot="$$header">
                                        </ng-transclude>
                                        <div ng-if="::$$title"
                                             class="modal-header">
                                            <h5 class="modal-title pull-left"
                                                ng-bind="::$$title"></h5>
                                        </div>
                                        <ng-transclude></ng-transclude>
                                        <ng-transclude ng-if="::$$footer"
                                                       ng-transclude-slot="$$footer">
                                        </ng-transclude>
                                    </div>
                                </div>
                            </div>`;

            return {
                restrict: 'A',
                scope: {
                    $$modal: "@",
                    $$title: "=?",
                    $$size: "@?",
                    $$onOpen: "&?"
                },
                transclude: {
                    '$$header': '?header',
                    '$$footer': '?footer'
                },
                template: template,
                link: function ($scope, $element, $attrs, $ctrl, $transclude) {
                    let $$directive = null,
                        element = $($element[0].childNodes[0]);

                    $$scopeProto.extend($scope.$parent);
                    $scope.$$header = $transclude.isSlotFilled('$$header');
                    $scope.$$footer = $transclude.isSlotFilled('$$footer');
                    $scope.$$size = $scope.$$size || 'md';
                    $scope.$$title = $scope.$$title || 'title';

                    function $$controller() {
                        if ($attrs.$$modal) {
                            $scope.$parent.$$set($attrs.$$modal, this);
                        }
                    }

                    $$controller.prototype.open = function () {
                        if ($attrs.$$onOpen) {
                            $scope.$$onOpen();
                        }
                        if (!$scope.$root.$$phase) {
                            $scope.$digest();
                        }                          
                        element.modal('show');                                             
                    }

                    $$controller.prototype.close = function () {
                        element.modal('hide');
                    }

                    $$controller.prototype.onClose = function (callback) {
                        element.on('hidden.bs.modal', function () {
                            element.off('hidden.bs.modal');
                            callback();
                        });
                    }
                          
                    return $$directive = new $$controller();
                }
            };
        }]);