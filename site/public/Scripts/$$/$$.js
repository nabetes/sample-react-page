﻿'use strict';
var $$ = new function $$prototype() {
    this.toArray = function () {
        var source = arguments.length == 1 ? arguments[0] : arguments;
        return Array.prototype.slice.call(source);
    }
};