﻿angular.module('$$lazyRender', ["$$scopeProto"])
    .directive('$$lazyRender', ["$$scopeProto", function ($$scopeProto) {
        return {
            transclude: 'element',
            priority: 1200, // High prio, so it runs before any other directive (changed needed for 1.2))
            terminal: true, // Prevents subsequent directives from being processed
            restrict: 'A',
            template: `<div class="page-loader" style="position: relative;">
                            <div class="page-loader__spinner">
                                <svg viewBox="25 25 50 50">
                                    <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
                                </svg>
                            </div>
                        </div>`,
            replace: true,
            scope: false,
            compile: function ($element, $$attr, linker, $trasnsclude) {
                /**
                * Compile function
                * @param {object} $scope: scope of the child element
                * @param {object} $iterStartElement:  child elment
                * @param {object} $attr: = {
                *   $$lazyRender: false
                * }
                */
                return function ($scope, $iterStartElement, $attr) {
                    $$scopeProto.extend($scope);
                    $scope.$$set($attr.$$lazyRender, function (callback) {
                        linker($scope, function (clone) {
                            $element.before(clone);
                            if (callback)
                                callback();
                            $element.remove();
                        });
                    });
                };
            }
        };
    }]);