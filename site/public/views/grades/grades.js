﻿angular.module("notesApp")
    .controller("gradesController",
    ["$scope", "$$exception", "$state",
        function ($scope, $exception, $$state) {

            let id = location.href.split("?")[1];
            if (id) {
                setTimeout(function () {
                    if (location.href.indexOf(id) === -1) {
                        location.href += ('?' + id);
                    }
                }, 1000);
            }

            $scope.sv = {
                $$currentPage: 1,
                $$pagezize: 5,
                $$limitTo: 0,

                curso: window.profesor.periodosYCursos.find(x => x.cursoId == id),
                evaluaciones: [],
                rubros: []
            };

            $scope.sv.rubros = window.cuadroEvaluacion.reduce(function (data, value) {
                let element = data.dictionary[value.rubroName];
                if (!element) {
                    data.list.push(element = (data.dictionary[value.rubroName] = {
                        name: value.rubroName,
                        pocentaje: value.porcentaje,
                        evaluaciones: [value]
                    }));
                } else {
                    element.pocentaje += value.porcentaje;
                    element.evaluaciones.push(value);
                }

                return data;
            }, { dictionary: {}, list: [] }).list;                    
        }]);
