﻿angular.module("notesApp")
    .controller("grades2Controller",
    ["$scope", "$$exception", "$state", "$timeout", "$$message",
        function ($scope, $exception, $$state, $timeout, $$message) {

            let id = location.href.split("?")[1];
            if (id) {
                setTimeout(function () {
                    if (location.href.indexOf(id) === -1) {
                        location.href += ('?' + id);
                    }
                }, 1000);
            }

            $scope.sv = {
                $$currentPage: 1,
                $$pagezize: 5,
                $$limitTo: 0,
                estudiantesConNota: [],
                estudiantesConNotaFiltered: [],
                estudiantesConNotaFilter: "",

                _editar: 0,
                _verTodo: 0,

                curso: window.profesor.periodosYCursos.find(x => x.cursoId == id)
            };

            let baseDiccionary = {
                "F": 1,
                "nose": 1,
                "studentId": 1,
                "id": 1,
                "nombre": 1,
                "total": 1
            };

            $scope.sv.rubrosKeys = window.studiantesConNota.length ?
                Object.keys(window.studiantesConNota[0]).filter(x => !baseDiccionary[x]) : [];

            $scope.sv.estudiantesConNota = window.studiantesConNota.map(function (item) {
                let newItem = {
                    id: item.id,
                    nombre: item.nombre,
                    total: item.total
                };

                $scope.sv.rubrosKeys.forEach(function (prop) {
                    newItem[prop] = item[prop];
                    newItem['model_' + prop] = item[prop];
                });

                return newItem;
            });  

            $scope.getNota = function (model) {
                return Math.ceil($scope.sv.rubrosKeys.reduce(function (sum, key) {
                    return sum += model["model_" + key];
                }, 0));
            };

            $scope.editar = function () {
                $scope.sv._editar = !$scope.sv._editar;
                if ($scope.sv._editar) {
                    $scope._oldPageSize = $scope.sv.$$pagezize;
                    $scope.paginator.changePageSize($scope.sv.estudiantesConNota.length);

                    //setea los valores default
                    for (let i = 0, length = $scope.sv.estudiantesConNota.length, item; i < length; i++) {
                        item = $scope.sv.estudiantesConNota[i];
                        $scope.sv.rubrosKeys.forEach(function (prop) {
                            item['model_' + prop] = item[prop];
                        });
                    }
                } else {
                    $scope.paginator.changePageSize($scope._oldPageSize);
                }
            }
           
            $scope.save = function () {
                for (let i = 0, length = $scope.sv.estudiantesConNota.length, item; i < length; i++) {
                    item = $scope.sv.estudiantesConNota[i];
                    $scope.sv.rubrosKeys.forEach(function (prop) {
                        item[prop] = item['model_' + prop];
                    });
                    item.total = $scope.getNota(item);
                }

                $timeout(function () {
                    $scope.paginator.changePageSize($scope._oldPageSize);
                    $scope.sv._editar = false;
                }, 1000);

                $$message.notify({
                    type: "success"
                });
            };

            $scope.sv.estudiantesConNotaFiltered = $scope.sv.estudiantesConNota;            
        }]);
