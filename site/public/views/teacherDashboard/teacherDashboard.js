﻿angular.module("notesApp")
    .controller("teacherDashboardController", ["$document", "$scope", "$timeout",
        function ($document, $scope, $timeout) {

            $scope.sv = {
                lazyrender: null,
                cuadroEvaluacion: [],
                events: []
            };

            $scope.sv.events = window.cuadroEvaluacion.map(function (item) {
                return {
                    title: item.rubroDisplay,
                    start: moment(item.fechaEvaluacion, "YYYY/MM/DD").format('YYYY-MM-DD'),
                    //url: 'http://google.com/',
                    className: window.cuadroEvaluacionColoresRubro[item.rubroName]
                };
            });

            $scope.ocultar = function (id) {
                $scope.sv.cuadroEvaluacion = $scope.sv.cuadroEvaluacion.map(x => {
                    x.hide = x.RubroEvaluacionId == id;
                    return x;
                });
            }

            angular.element($document).ready(function () {
                $scope.sv.lazyrender(function () {

                    let date = new Date("Mon, 01 Apr 2019 06:00:00 GMT");
                    let momentdate = moment(date);
                    
                    var mYear = momentdate.format('YYYY');
                    var mDay = momentdate.format('dddd, MMM D');
                    $('.widget-calendar__year').html(mYear);
                    $('.widget-calendar__day').html(mDay);

                    $('.widget-calendar__body').fullCalendar({
                        lang: 'es',
                        contentHeight: 'auto',
                        theme: false,
                        buttonIcons: {
                            prev: ' zmdi zmdi-long-arrow-left',
                            next: ' zmdi zmdi-long-arrow-right'
                        },
                        header: {
                            right: 'next',
                            center: 'title, ',
                            left: 'prev'
                        },
                        defaultDate: date,
                        editable: false,
                        events: $scope.sv.events
                    });

                    $timeout(function () {
                        $scope.sv.cuadroEvaluacion = window.cuadroEvaluacion.map(function (event) {
                            return {
                                RubroEvaluacionId: event.RubroEvaluacionId,
                                rubroDisplay: `${event.rubroDisplay}, ${event.porcentaje}%`,
                                fechaEvaluacion: moment(event.fechaEvaluacion, "YYYY/MM/DD").format('YYYY mm dd'),
                                rubroName: `${event.rubroName.substring(0, 2).toUpperCase()}...`,
                                className: window.cuadroEvaluacionColoresRubro[event.rubroName]
                            };
                        });
                    }, 0);                                      
                });                 
            });                   
        }]);
//$(".scrollbar-inner")[0] && $(".scrollbar-inner").scrollbar().scrollLock()

//$('.clickdemo').sparkline();
//$('.clickdemo').bind('sparklineClick', function (ev) {
//    var sparkline = ev.sparklines[0],
//        region = sparkline.getCurrentRegionFields();
//    alert("Clicked on x=" + region.x + " y=" + region.y);
//});

//tooltipFormatter: function (sp, options, fields) {
//    var format = $.spformat('<div class="jqsfield"><span style="color: {{color}}">&#9679;</span> {{myprefix}} {{value}}</div>');
//    var result = '';
//    $.each(fields, function (i, field) {
//        field.myprefix = options.get('myPrefixes')[i];
//        result += format.render(field, options.get('tooltipValueLookups'), options);
//    })
//    return result;
//},