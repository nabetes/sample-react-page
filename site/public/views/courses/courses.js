﻿angular.module("notesApp")
    .controller("coursesController",
    ["$scope", "$$exception",
        function ($scope, $$exception) {
            $scope.sv = {
                $$currentPage: 1,
                $$pagezize: 5,
                $$limitTo: 0,
                courses: [],
                coursesFiltered: [],
                coursesFilter: "",

                _showResume: true
            };
           
            $scope.sv.courses = window.profesor.periodosYCursosDetalle;      
            $scope.sv.coursesFiltered = $scope.sv.courses;
        }]);
