﻿angular.module("notesApp")
    .controller("studentsController", ["$scope", "$timeout", "$document",
        "$$exception", '$$message', "$http", "$state",
        function ($scope, $timeout, $document,
            $$exception, $$message, $$http, $$state) {
            let $$this = new $$controller();

            function $$controller() {
                $scope.sv = {
                    lazyrender: null,

                    $$currentPage: 1,
                    $$pagezize: 5,
                    $$limitTo: 0,
                    students: [],
                    studentsFiltered: [],
                    studentsFilter: "",

                    _verTodo: 0,

                    _showResume: true
                };

                //fake
                $scope.sv.students = window.studiantesConCorreo;
                $scope.sv.studentsFiltered = $scope.sv.students;

                $scope.getNotaStudianteNota = function (id) {
                    return window.studiantesConNotaDictionary[id].total;
                }

                $scope.getAprovados = function () {
                    return window.studiantesConNota.reduce(function (sum, item) {
                        if (item.total >= 70) {
                            sum++;
                        }
                        return sum;
                    }, 0);
                }

                $scope.getReprovados = function () {
                    return window.studiantesConNota.reduce(function (sum, item) {
                        if (item.total < 70) {
                            sum++;
                        }
                        return sum;
                    }, 0);
                }

                $scope.sv.cursos = window.profesor.periodosYCursos;

                angular.element($document).ready(function () {
                    $scope.sv.lazyrender(function () {
                        let periodos = ['2018-1', '2018-2', '2018-3', "2019-1", "2019-2"],
                            cursos = ['P1-Grupo 1', 'P1-Grupo 2', 'P1-Grupo 3', "P2-Grupo 1", "P2-Grupo 2"]
                        flag = false;

                        $('.sparkline-bar-stats').sparkline('html', {
                            type: 'bar',
                            height: 36,
                            barWidth: 10,
                            barColor: '#fff',
                            barSpacing: 5,
                            zeroAxis: false,
                            chartRangeMin: 0,
                            tooltipFormatter: function (sp, options, fields) {
                                let flag = !!sp.$el.parents(".__first").length,
                                    field = fields[0],
                                    index = field.offset,
                                    format = $.spformat(`<div class="jqsfield"><b>{{periodo}}</b> - <b>{{value}}</b></div>`);
                                field.periodo = flag ? periodos[index] : cursos[index];
                                return format.render(field, options.get('tooltipValueLookups'), options);
                            },
                        });
                    });
                });
            }
        }]);



















//["a", "b", "c", "d"].forEach((x, index) => { $.worker.send(x).promise.then(x => { console.log(x.data); }) });

//$.worker.overrideAfterLast("e").promise.then(x => { console.log(x.data) });


$(document).ready(function () {
    //$(".scrollbar-inner").scrollbar().scrollLock()

    // Active Stat
    $('body').on('focus', '.search__text', function () {
        $(this).closest('.search-input').addClass('search--focus');
    });

    // Clear
    $('body').on('blur', '.search__text', function () {
        $(this).val('');
        $(this).closest('.search-input').removeClass('search--focus');
    });
});


//$$http.post("http://localhost:52239/api/login/forgotPassword", {

//})
//    .then(function () {
//        $$message.notify({
//            type: "success"
//        });
//    }, function (ex, b, error, d) {
//        $scope.frmForgot.$$form.inputsAddClass("has-danger");
//        $$exception.show(error || ex);
//    });

/*Worker*/
//let worker = new $$worker({
//    scripts: [`${origin}/Scripts/$$/$$workerHttp.js?v=0`],
//    content: function () {
//        function $$do(event) {
//            let $this = this;
//            //let tonto = bruto;
//            //let someNumber = 0;
//            //for (var i = 0; i < 100000000; i++) {
//            //    someNumber += 1;
//            //}
//            //this.postMessage(event.data);
//            $http.get("http://localhost:52239/api/settings/getUserSettings")
//                .then(function () {
//                    this.$$send("success");
//                }, function (error) {
//                    this.$$sendError(error);
//                });
//        }
//    }
//});

//worker.send("hola").promise.then(function () {
//    $$message.notify({
//        type: "success"
//    });
//});